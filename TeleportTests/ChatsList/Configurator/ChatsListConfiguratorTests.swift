//
//  ChatsListChatsListConfiguratorTests.swift
//  Teleport
//
//  Created by VoidPtr on 20/03/2018.
//  Copyright © 2018 kekussoft. All rights reserved.
//

import XCTest

class ChatsListModuleConfiguratorTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testConfigureModuleForViewController() {

        //given
        let viewController = ChatsListViewControllerMock()
        let configurator = ChatsListModuleConfigurator()

        //when
        configurator.configureModuleForViewInput(viewInput: viewController)

        //then
        XCTAssertNotNil(viewController.output, "ChatsListViewController is nil after configuration")
        XCTAssertTrue(viewController.output is ChatsListPresenter, "output is not ChatsListPresenter")

        let presenter: ChatsListPresenter = viewController.output as! ChatsListPresenter
        XCTAssertNotNil(presenter.view, "view in ChatsListPresenter is nil after configuration")
        XCTAssertNotNil(presenter.router, "router in ChatsListPresenter is nil after configuration")
        XCTAssertTrue(presenter.router is ChatsListRouter, "router is not ChatsListRouter")

        let interactor: ChatsListInteractor = presenter.interactor as! ChatsListInteractor
        XCTAssertNotNil(interactor.output, "output in ChatsListInteractor is nil after configuration")
    }

    class ChatsListViewControllerMock: ChatsListViewController {

        var setupInitialStateDidCall = false

        override func setupInitialState() {
            setupInitialStateDidCall = true
        }
    }
}

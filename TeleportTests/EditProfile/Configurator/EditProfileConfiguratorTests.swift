//
//  EditProfileEditProfileConfiguratorTests.swift
//  Teleport
//
//  Created by VoidPtr on 20/03/2018.
//  Copyright © 2018 kekussoft. All rights reserved.
//

import XCTest

class EditProfileModuleConfiguratorTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testConfigureModuleForViewController() {

        //given
        let viewController = EditProfileViewControllerMock()
        let configurator = EditProfileModuleConfigurator()

        //when
        configurator.configureModuleForViewInput(viewInput: viewController)

        //then
        XCTAssertNotNil(viewController.output, "EditProfileViewController is nil after configuration")
        XCTAssertTrue(viewController.output is EditProfilePresenter, "output is not EditProfilePresenter")

        let presenter: EditProfilePresenter = viewController.output as! EditProfilePresenter
        XCTAssertNotNil(presenter.view, "view in EditProfilePresenter is nil after configuration")
        XCTAssertNotNil(presenter.router, "router in EditProfilePresenter is nil after configuration")
        XCTAssertTrue(presenter.router is EditProfileRouter, "router is not EditProfileRouter")

        let interactor: EditProfileInteractor = presenter.interactor as! EditProfileInteractor
        XCTAssertNotNil(interactor.output, "output in EditProfileInteractor is nil after configuration")
    }

    class EditProfileViewControllerMock: EditProfileViewController {

        var setupInitialStateDidCall = false

        override func setupInitialState() {
            setupInitialStateDidCall = true
        }
    }
}

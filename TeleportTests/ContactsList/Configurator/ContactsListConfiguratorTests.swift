//
//  ContactsListContactsListConfiguratorTests.swift
//  Teleport
//
//  Created by VoidPtr on 20/03/2018.
//  Copyright © 2018 kekussoft. All rights reserved.
//

import XCTest

class ContactsListModuleConfiguratorTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testConfigureModuleForViewController() {

        //given
        let viewController = ContactsListViewControllerMock()
        let configurator = ContactsListModuleConfigurator()

        //when
        configurator.configureModuleForViewInput(viewInput: viewController)

        //then
        XCTAssertNotNil(viewController.output, "ContactsListViewController is nil after configuration")
        XCTAssertTrue(viewController.output is ContactsListPresenter, "output is not ContactsListPresenter")

        let presenter: ContactsListPresenter = viewController.output as! ContactsListPresenter
        XCTAssertNotNil(presenter.view, "view in ContactsListPresenter is nil after configuration")
        XCTAssertNotNil(presenter.router, "router in ContactsListPresenter is nil after configuration")
        XCTAssertTrue(presenter.router is ContactsListRouter, "router is not ContactsListRouter")

        let interactor: ContactsListInteractor = presenter.interactor as! ContactsListInteractor
        XCTAssertNotNil(interactor.output, "output in ContactsListInteractor is nil after configuration")
    }

    class ContactsListViewControllerMock: ContactsListViewController {

        var setupInitialStateDidCall = false

        override func setupInitialState() {
            setupInitialStateDidCall = true
        }
    }
}

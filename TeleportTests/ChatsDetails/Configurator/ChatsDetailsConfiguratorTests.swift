//
//  ChatsDetailsChatsDetailsConfiguratorTests.swift
//  Teleport
//
//  Created by VoidPtr on 20/03/2018.
//  Copyright © 2018 kekussoft. All rights reserved.
//

import XCTest

class ChatsDetailsModuleConfiguratorTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testConfigureModuleForViewController() {

        //given
        let viewController = ChatsDetailsViewControllerMock()
        let configurator = ChatsDetailsModuleConfigurator()

        //when
        configurator.configureModuleForViewInput(viewInput: viewController)

        //then
        XCTAssertNotNil(viewController.output, "ChatsDetailsViewController is nil after configuration")
        XCTAssertTrue(viewController.output is ChatsDetailsPresenter, "output is not ChatsDetailsPresenter")

        let presenter: ChatsDetailsPresenter = viewController.output as! ChatsDetailsPresenter
        XCTAssertNotNil(presenter.view, "view in ChatsDetailsPresenter is nil after configuration")
        XCTAssertNotNil(presenter.router, "router in ChatsDetailsPresenter is nil after configuration")
        XCTAssertTrue(presenter.router is ChatsDetailsRouter, "router is not ChatsDetailsRouter")

        let interactor: ChatsDetailsInteractor = presenter.interactor as! ChatsDetailsInteractor
        XCTAssertNotNil(interactor.output, "output in ChatsDetailsInteractor is nil after configuration")
    }

    class ChatsDetailsViewControllerMock: ChatsDetailsViewController {

        var setupInitialStateDidCall = false

        override func setupInitialState() {
            setupInitialStateDidCall = true
        }
    }
}

//
//  ChannelsDetailsChannelsDetailsConfiguratorTests.swift
//  Teleport
//
//  Created by VoidPtr on 20/03/2018.
//  Copyright © 2018 kekussoft. All rights reserved.
//

import XCTest

class ChannelsDetailsModuleConfiguratorTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testConfigureModuleForViewController() {

        //given
        let viewController = ChannelsDetailsViewControllerMock()
        let configurator = ChannelsDetailsModuleConfigurator()

        //when
        configurator.configureModuleForViewInput(viewInput: viewController)

        //then
        XCTAssertNotNil(viewController.output, "ChannelsDetailsViewController is nil after configuration")
        XCTAssertTrue(viewController.output is ChannelsDetailsPresenter, "output is not ChannelsDetailsPresenter")

        let presenter: ChannelsDetailsPresenter = viewController.output as! ChannelsDetailsPresenter
        XCTAssertNotNil(presenter.view, "view in ChannelsDetailsPresenter is nil after configuration")
        XCTAssertNotNil(presenter.router, "router in ChannelsDetailsPresenter is nil after configuration")
        XCTAssertTrue(presenter.router is ChannelsDetailsRouter, "router is not ChannelsDetailsRouter")

        let interactor: ChannelsDetailsInteractor = presenter.interactor as! ChannelsDetailsInteractor
        XCTAssertNotNil(interactor.output, "output in ChannelsDetailsInteractor is nil after configuration")
    }

    class ChannelsDetailsViewControllerMock: ChannelsDetailsViewController {

        var setupInitialStateDidCall = false

        override func setupInitialState() {
            setupInitialStateDidCall = true
        }
    }
}

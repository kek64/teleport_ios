//
//  TypePhoneNumberTypePhoneNumberConfiguratorTests.swift
//  Teleport
//
//  Created by VoidPtr on 20/03/2018.
//  Copyright © 2018 kekussoft. All rights reserved.
//

import XCTest

class TypePhoneNumberModuleConfiguratorTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testConfigureModuleForViewController() {

        //given
        let viewController = TypePhoneNumberViewControllerMock()
        let configurator = TypePhoneNumberModuleConfigurator()

        //when
        configurator.configureModuleForViewInput(viewInput: viewController)

        //then
        XCTAssertNotNil(viewController.output, "TypePhoneNumberViewController is nil after configuration")
        XCTAssertTrue(viewController.output is TypePhoneNumberPresenter, "output is not TypePhoneNumberPresenter")

        let presenter: TypePhoneNumberPresenter = viewController.output as! TypePhoneNumberPresenter
        XCTAssertNotNil(presenter.view, "view in TypePhoneNumberPresenter is nil after configuration")
        XCTAssertNotNil(presenter.router, "router in TypePhoneNumberPresenter is nil after configuration")
        XCTAssertTrue(presenter.router is TypePhoneNumberRouter, "router is not TypePhoneNumberRouter")

        let interactor: TypePhoneNumberInteractor = presenter.interactor as! TypePhoneNumberInteractor
        XCTAssertNotNil(interactor.output, "output in TypePhoneNumberInteractor is nil after configuration")
    }

    class TypePhoneNumberViewControllerMock: TypePhoneNumberViewController {

        var setupInitialStateDidCall = false

        override func setupInitialState() {
            setupInitialStateDidCall = true
        }
    }
}

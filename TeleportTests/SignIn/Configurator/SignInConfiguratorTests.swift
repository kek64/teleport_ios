//
//  SignInSignInConfiguratorTests.swift
//  Teleport
//
//  Created by VoidPtr on 20/03/2018.
//  Copyright © 2018 kekussoft. All rights reserved.
//

import XCTest

class SignInModuleConfiguratorTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testConfigureModuleForViewController() {

        //given
        let viewController = SignInViewControllerMock()
        let configurator = SignInModuleConfigurator()

        //when
        configurator.configureModuleForViewInput(viewInput: viewController)

        //then
        XCTAssertNotNil(viewController.output, "SignInViewController is nil after configuration")
        XCTAssertTrue(viewController.output is SignInPresenter, "output is not SignInPresenter")

        let presenter: SignInPresenter = viewController.output as! SignInPresenter
        XCTAssertNotNil(presenter.view, "view in SignInPresenter is nil after configuration")
        XCTAssertNotNil(presenter.router, "router in SignInPresenter is nil after configuration")
        XCTAssertTrue(presenter.router is SignInRouter, "router is not SignInRouter")

        let interactor: SignInInteractor = presenter.interactor as! SignInInteractor
        XCTAssertNotNil(interactor.output, "output in SignInInteractor is nil after configuration")
    }

    class SignInViewControllerMock: SignInViewController {

        var setupInitialStateDidCall = false

        override func setupInitialState() {
            setupInitialStateDidCall = true
        }
    }
}

//
//  MessangerMessangerConfiguratorTests.swift
//  Teleport
//
//  Created by VoidPtr on 20/03/2018.
//  Copyright © 2018 kekussoft. All rights reserved.
//

import XCTest

class MessangerModuleConfiguratorTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testConfigureModuleForViewController() {

        //given
        let viewController = MessangerViewControllerMock()
        let configurator = MessangerModuleConfigurator()

        //when
        configurator.configureModuleForViewInput(viewInput: viewController)

        //then
        XCTAssertNotNil(viewController.output, "MessangerViewController is nil after configuration")
        XCTAssertTrue(viewController.output is MessangerPresenter, "output is not MessangerPresenter")

        let presenter: MessangerPresenter = viewController.output as! MessangerPresenter
        XCTAssertNotNil(presenter.view, "view in MessangerPresenter is nil after configuration")
        XCTAssertNotNil(presenter.router, "router in MessangerPresenter is nil after configuration")
        XCTAssertTrue(presenter.router is MessangerRouter, "router is not MessangerRouter")

        let interactor: MessangerInteractor = presenter.interactor as! MessangerInteractor
        XCTAssertNotNil(interactor.output, "output in MessangerInteractor is nil after configuration")
    }

    class MessangerViewControllerMock: MessangerViewController {

        var setupInitialStateDidCall = false

        override func setupInitialState() {
            setupInitialStateDidCall = true
        }
    }
}

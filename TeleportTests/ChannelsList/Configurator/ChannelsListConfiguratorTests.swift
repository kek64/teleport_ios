//
//  ChannelsListChannelsListConfiguratorTests.swift
//  Teleport
//
//  Created by VoidPtr on 20/03/2018.
//  Copyright © 2018 kekussoft. All rights reserved.
//

import XCTest

class ChannelsListModuleConfiguratorTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testConfigureModuleForViewController() {

        //given
        let viewController = ChannelsListViewControllerMock()
        let configurator = ChannelsListModuleConfigurator()

        //when
        configurator.configureModuleForViewInput(viewInput: viewController)

        //then
        XCTAssertNotNil(viewController.output, "ChannelsListViewController is nil after configuration")
        XCTAssertTrue(viewController.output is ChannelsListPresenter, "output is not ChannelsListPresenter")

        let presenter: ChannelsListPresenter = viewController.output as! ChannelsListPresenter
        XCTAssertNotNil(presenter.view, "view in ChannelsListPresenter is nil after configuration")
        XCTAssertNotNil(presenter.router, "router in ChannelsListPresenter is nil after configuration")
        XCTAssertTrue(presenter.router is ChannelsListRouter, "router is not ChannelsListRouter")

        let interactor: ChannelsListInteractor = presenter.interactor as! ChannelsListInteractor
        XCTAssertNotNil(interactor.output, "output in ChannelsListInteractor is nil after configuration")
    }

    class ChannelsListViewControllerMock: ChannelsListViewController {

        var setupInitialStateDidCall = false

        override func setupInitialState() {
            setupInitialStateDidCall = true
        }
    }
}

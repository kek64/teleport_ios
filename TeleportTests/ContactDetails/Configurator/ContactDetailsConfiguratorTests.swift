//
//  ContactDetailsContactDetailsConfiguratorTests.swift
//  Teleport
//
//  Created by VoidPtr on 20/03/2018.
//  Copyright © 2018 kekussoft. All rights reserved.
//

import XCTest

class ContactDetailsModuleConfiguratorTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testConfigureModuleForViewController() {

        //given
        let viewController = ContactDetailsViewControllerMock()
        let configurator = ContactDetailsModuleConfigurator()

        //when
        configurator.configureModuleForViewInput(viewInput: viewController)

        //then
        XCTAssertNotNil(viewController.output, "ContactDetailsViewController is nil after configuration")
        XCTAssertTrue(viewController.output is ContactDetailsPresenter, "output is not ContactDetailsPresenter")

        let presenter: ContactDetailsPresenter = viewController.output as! ContactDetailsPresenter
        XCTAssertNotNil(presenter.view, "view in ContactDetailsPresenter is nil after configuration")
        XCTAssertNotNil(presenter.router, "router in ContactDetailsPresenter is nil after configuration")
        XCTAssertTrue(presenter.router is ContactDetailsRouter, "router is not ContactDetailsRouter")

        let interactor: ContactDetailsInteractor = presenter.interactor as! ContactDetailsInteractor
        XCTAssertNotNil(interactor.output, "output in ContactDetailsInteractor is nil after configuration")
    }

    class ContactDetailsViewControllerMock: ContactDetailsViewController {

        var setupInitialStateDidCall = false

        override func setupInitialState() {
            setupInitialStateDidCall = true
        }
    }
}

//
//  ViewController.swift
//  CommunicationLayer
//
//  Created by VoidPtr on 3/27/18.
//  Copyright © 2018 VoidPtr. All rights reserved.
//

import UIKit
import CoreLocation

class ViewController: UIViewController {

    // MARK: - Properties

    @IBOutlet weak var messageText: UITextField!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var pingButton: UIButton!
    @IBOutlet weak var pongButton: UIButton!
    @IBOutlet weak var connectButton: UIButton!
    @IBOutlet weak var disconnectButton: UIButton!

    let urlString = "wss://echo.websocket.org:8000" // "ws://localhost:8000"
    var sockets: WebSocketService!
    let manager = CLLocationManager()

    // MARK: - Life-Cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        messageText.text = """
                            // MARK: - Life-Cycle
                            init(request: URLRequest) {
                            self.request = request
                            let host = request.url!.host!
                            let port = request.url!.port!
                            self.reconnectionPolicy = .never
                            self.communicationLayer = CommunicationLayer(host: host,
                            port: port, serializer: CommunicationLayerSerializer.shared)
                            self.communicationLayer.delegate = self
                            self.configureTimer()
                            } // MARK: - Private
                            private func configureTimer() {
                            switch reconnectionPolicy {
                            case .always, .never:
                            break case .periodicaly(let fireTime):
                            reconnectTimer = Timer(timeInterval: fireTime, target: self, selector: #selector(reconnectIfNeeded(_:)), userInfo: nil, repeats: true)
                            RunLoop.main.add(reconnectTimer, forMode: .commonModes)
                            } }
                            """
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        manager.requestAlwaysAuthorization()
        manager.startMonitoringSignificantLocationChanges()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Actions

    @IBAction func actionSend(_ sender: Any) {
        self.sockets.send(message: messageText.text ?? "Simple message")
    }

    @IBAction func actionPing(_ sender: Any) {
        sockets.sendPing()
    }

    @IBAction func actionPong(_ sender: Any) {
        sockets.sendPong()
    }

    @IBAction func actionConnect(_ sender: Any) {
        if sockets == nil {
            let request = URLRequest(url: URL(string: urlString)!)
            sockets = WebSocketService(request: request)
            return
        }

        sockets.connect()
    }

    @IBAction func actionDisconnect(_ sender: Any) {
        sockets.disconnect()
    }
    
}


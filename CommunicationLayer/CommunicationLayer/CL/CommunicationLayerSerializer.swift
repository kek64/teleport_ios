//
//  CommunicationLayerSerializer.swift
//  CommunicationLayer
//
//  Created by VoidPtr on 3/27/18.
//  Copyright © 2018 VoidPtr. All rights reserved.
//

import CFNetwork
import Foundation

protocol CommunicationLayerSerializerProtocol: class {
    func deserialize(stream: InputStream, onSuccess: @escaping (Any?) -> Void, onError: @escaping (Error) -> Void)
    func serialize(stream: OutputStream, message: Any?, onSuccess: @escaping () -> Void, onError: @escaping (Error) -> Void)
}

final class CommunicationLayerSerializer: CommunicationLayerSerializerProtocol {

    static let shared = CommunicationLayerSerializer()

    private let bufferSize = 1024
    private var buffer: Array<UInt8>

    init() {
        self.buffer = Array<UInt8>(repeating: 0, count: bufferSize)
    }

    func deserialize(stream: InputStream, onSuccess: @escaping (Any?) -> Void, onError: @escaping (Error) -> Void) {
        memset(&buffer, 0, buffer.count)
        let bytesRead = stream.read(&buffer, maxLength: bufferSize)

        if bytesRead >= 0 {
            if let response = String(bytes: buffer, encoding: .utf8) {
                onSuccess(response)
            } else if let response = String(bytes: buffer, encoding: .ascii) {
                onSuccess(response)
            } else {
                onError(NSError(domain: "", code: 0, userInfo: ["Error" : "No valid response"]))
            }
        }
    }

    func serialize(stream: OutputStream, message: Any?, onSuccess: @escaping () -> Void, onError: @escaping (Error) -> Void) {
        guard let unwrappedMessage = message as? String else {
            onError(NSError(domain: "", code: 0, userInfo: ["Error" : "Try send empty message"]))
            return
        }

        var data = [UInt8]()
        for byte in unwrappedMessage.utf8 {
            data.append(byte)
        }

        if stream.write(&data, maxLength: data.count) == -1 {
            onError(NSError(domain: "", code: 0, userInfo: ["Error" : "Failed write data to stream."]))
        } else {
            onSuccess()
        }
    }
}

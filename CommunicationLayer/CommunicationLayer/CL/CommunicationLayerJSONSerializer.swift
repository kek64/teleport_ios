//
//  CommunicationLayerJSONSerializer.swift
//  CommunicationLayer
//
//  Created by VoidPtr on 3/27/18.
//  Copyright © 2018 VoidPtr. All rights reserved.
//

import CFNetwork
import Foundation

final class CommunicationLayerJSONSerializer: CommunicationLayerSerializerProtocol {

    private struct Constants {
        static let bufferSize = 1024
        static let encoding = String.Encoding.utf8
    }

    func deserialize(stream: InputStream, onSuccess: @escaping (Any?) -> Void, onError: @escaping (Error) -> Void) {
        var buffer = Array<UInt8>(repeating: 0, count: Constants.bufferSize)

        let bytesRead = stream.read(&buffer, maxLength: Constants.bufferSize)
        if bytesRead >= 0 {
            if let response = NSString(bytes: &buffer, length: bytesRead, encoding: Constants.encoding.rawValue),
                let responseData = response.data(using: Constants.encoding.rawValue) {
                do {
                    if let message = try JSONSerialization.jsonObject(with: responseData, options: []) as? [String: Any] {
                        onSuccess(message)
                    } else {
                        onSuccess(nil)
                    }

                } catch let error {
                    onError(error)
                }
            }
        }
    }

    func serialize(stream: OutputStream, message: Any?, onSuccess: @escaping () -> Void, onError: @escaping (Error) -> Void) {
        guard let unwrappedMessage = message else {
            onError(NSError(domain: "", code: 0, userInfo: ["Error" : "Try send empty message"]))
            return
        }

        var err: NSError?
        let result = JSONSerialization.writeJSONObject(unwrappedMessage, to: stream, options: [], error: &err)
        if result <= 0, let error = err {
            onError(error)
        } else {
            onSuccess()
        }
    }
}


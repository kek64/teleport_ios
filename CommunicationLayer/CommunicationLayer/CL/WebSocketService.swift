//
//  WebSocketService.swift
//  Teleport
//
//  Created by VoidPtr on 3/26/18.
//  Copyright © 2018 VoidPtr. All rights reserved.
//

import Foundation
import UIKit

public enum CommunicationServiceReconnectionPolicy {
    case always
    case never
    case periodicaly(fireTime: TimeInterval)
}

extension String {
    func splitByLength(_ length: Int) -> [String] {
        var result = [String]()
        var collectedCharacters = [Character]()
        collectedCharacters.reserveCapacity(length)
        var count = 0

        for character in self {
            collectedCharacters.append(character)
            count += 1
            if (count == length) {
                // Reached the desired length
                count = 0
                result.append(String(collectedCharacters))
                collectedCharacters.removeAll(keepingCapacity: true)
            }
        }

        // Append the remainder
        if !collectedCharacters.isEmpty {
            result.append(String(collectedCharacters))
        }

        return result
    }

    func rawData() -> [UInt8] {
        var data: [UInt8] = []
        for byte in utf8 {
            data.append( byte )
        }
        return data
    }

}

final class WebSocketService {

    // MARK: - Properties

    enum OperationCodes: UInt8 {
        case text = 0x1
        case binary = 0x2
        case disconnect = 0x8
        case ping = 0x9
        case pong = 0xA
    }

    struct Fin {
        static let single = UInt8(1 << 7)
        static let continuation = UInt8(0x0)
    }

    enum DisconnectCodes: UInt {
        case normal = 1000
        case remoteDisappear = 1001
        case remoteError = 1002
        case remoteInvalidData = 1003
    }
    
    private let communicationLayer: CommunicationLayer
    private let reconnectionPolicy: CommunicationServiceReconnectionPolicy
    private var reconnectTimer: Timer!

    private var request: URLRequest!

    // MARK: - Life-Cycle

    init(request: URLRequest) {
        self.request = request

        let host = request.url!.host!
        let port = request.url!.port!

        self.reconnectionPolicy = .never
        self.communicationLayer = CommunicationLayer(host: host,
                                                     port: port,
                                                     serializer: CommunicationLayerSerializer.shared)
        self.communicationLayer.delegate = self
        self.configureTimer()
    }

    // MARK: - Private

    private func configureTimer() {
        switch reconnectionPolicy {
        case .always, .never:
            break
        case .periodicaly(let fireTime):
            reconnectTimer = Timer(timeInterval: fireTime, target: self, selector: #selector(reconnectIfNeeded(_:)), userInfo: nil, repeats: true)
            RunLoop.main.add(reconnectTimer, forMode: .commonModes)
        }
    }

    @objc
    private func reconnectIfNeeded(_ timer: Timer?) {
        if !communicationLayer.isConnected && !communicationLayer.isTryingConnect {
            print("[Timer] - Try reconnect")
            communicationLayer.establishConnect()
            performHandshake()
        }
    }

    private func dump(byte: UInt8, radix: Int) -> String {
        let string = String(byte, radix: radix, uppercase: true)
        var padded = string
        for _ in 0 ..< (8 - string.count) {
            padded = "0" + padded
        }
        return padded
    }

    // MARK: - Public

    func send(message: String) {
        switch reconnectionPolicy {
        case .always:
            reconnectIfNeeded(nil)
        default:
            break
        }

        let messageLength = message.count

        if messageLength >= 126 {
            let chunks = message.splitByLength(120)
            for (index, chunk) in chunks.enumerated() {
                let headerChunk = OperationCodes.text.rawValue ^ ( index == chunks.count - 1 ? Fin.single : Fin.continuation)
                print( "chunk number :: ", index)
                print( "header chunk :: ", dump(byte: headerChunk, radix: 16))
                print( "msg size :: ", dump(byte: UInt8(chunk.count), radix: 16))
                let data: [UInt8] = [ headerChunk, UInt8(chunk.count) ] + chunk.rawData()
                communicationLayer.write(raw: data)
            }

        } else {
            let headerChunk = OperationCodes.text.rawValue ^ Fin.single
            print( "header chunk :: ", dump(byte: headerChunk, radix: 16))
            print( "msg size :: ", dump(byte: UInt8(messageLength), radix: 16))
            let data: [UInt8] = [ headerChunk, UInt8(messageLength) ] + message.rawData()
            communicationLayer.write(raw: data)
        }
    }

    func connect() {
        communicationLayer.disconnect()
        communicationLayer.establishConnect()
    }

    func disconnect() {
        var data: [UInt8] = [OperationCodes.disconnect.rawValue]
        let dataString = String(describing: DisconnectCodes.normal)
        data += dataString.rawData()
        communicationLayer.write(raw: data)
        communicationLayer.disconnect()
    }

}

// MARK: - CommunicationLayerDelegate
extension WebSocketService: CommunicationLayerDelegate {

    func didConnect(stream: Stream) {
        print("Success connected")

        if let _ = stream as? OutputStream {
            performHandshake()
        }
    }

    func didDisconnet(stream: Stream, message: String) {
        switch reconnectionPolicy {
        case .always:
            reconnectIfNeeded(nil)
        default:
            break
        }

        print("Disconnected with message :: \(message)")
    }

    func didReceiveMessage(stream: Stream, message: Any) {
        print("\nReceive message :: \(message)\n")

        guard let message = message as? String else {
            return
        }

        // check handshake response
        DispatchQueue.main.async {
            if let delegate = UIApplication.shared.delegate as? AppDelegate {
                if let controller = delegate.window?.rootViewController {
                    controller.showMessage("Receive message", message.description)
                }
            }
        }
    }

    func didDisconnect() {
        print("Disconnect")
    }

}

// MARK: - WebSocketProtocol
extension WebSocketService {

    // send HTTP handshake
    func performHandshake() {
        request.setValue(request.url?.host ?? "localhost", forHTTPHeaderField: "Host")
        request.setValue("websocket", forHTTPHeaderField: "Upgrade")
        request.setValue("upgrade", forHTTPHeaderField: "Connection")
        request.setValue("localhost", forHTTPHeaderField: "Origin")
        request.setValue("chat, superchat", forHTTPHeaderField: "Sec-WebSocket-Protocol")
        request.setValue("x3JJHMbDL1EzLkh9GBhXDw==", forHTTPHeaderField: "Sec-WebSocket-Key")
        request.setValue("13", forHTTPHeaderField: "Sec-WebSocket-Version")

        var requestString = "GET /chat HTTP/1.1\r\n" // \(request.url!.host!):\(request.url!.port!)
        for key in request.allHTTPHeaderFields!.keys {
            if let val = request.value(forHTTPHeaderField: key) {
                requestString += "\(key): \(val)\r\n"
            }
        }
        requestString += "\r\n\r\n"

        let header = requestString.rawData()
        print("\n\(requestString)\n")
        communicationLayer.write(raw: header)
    }

    func onDisconnect(outputStream: OutputStream) {

    }

    func sendPing() {
        let opcode = OperationCodes.ping.rawValue ^ Fin.single
        communicationLayer.write(raw: [opcode, 0x0] ) //[opcode, 0x01, 0x48])
    }

    func sendPong() {
        let opcode = OperationCodes.pong.rawValue ^ Fin.single
        communicationLayer.write(raw: [opcode, 0x0]) // [opcode, 0x01, 0x48])
    }

}

//
//  CommunicationLayer.swift
//  Teleport
//
//  Created by VoidPtr on 3/20/18.
//  Copyright © 2018 VoidPtr. All rights reserved.
//

import Foundation
import CFNetwork

protocol CommunicationLayerDelegate: class {
    func didConnect(stream: Stream)
    func didDisconnet(stream: Stream, message: String)
    func didReceiveMessage(stream: Stream, message: Any)
    func didDisconnect()
}

final class CommunicationLayer: NSObject {

    // MARK: - Properties

    private var inputStream: InputStream?
    private var outputStream: OutputStream?
    private var serialQueue = DispatchQueue(label: "com.kekussoft.teleport", qos: .default, attributes: .concurrent)
    private var streamQueue = DispatchQueue(label: "com.kekussoft.teleport.stream.queue", qos: .default, attributes: .concurrent)
    private var serializer: CommunicationLayerSerializerProtocol

    private var messageQueue: [ Any ] = []
    private var host: String
    private var port: Int

    var isConnected: Bool = false
    var isTryingConnect: Bool = false
    weak var delegate: CommunicationLayerDelegate?

    // MARK: - Life-Cycle

    init(host: String, port: Int, serializer: CommunicationLayerSerializerProtocol, delegate: CommunicationLayerDelegate? = nil) {

        self.host = host
        self.port = port
        self.serializer = serializer
        self.delegate = delegate
        super.init()
        self.establishConnect()
    }

    deinit {
        disconnect()
    }

    // MARK: - Public

    func establishConnect() {
        isTryingConnect = true
       // Stream.getStreamsToHost(withName: host, port: port, inputStream: &inputStream, outputStream: &outputStream)

        var readStream: Unmanaged<CFReadStream>?
        var writeStream: Unmanaged<CFWriteStream>?

        CFStreamCreatePairWithSocketToHost(kCFAllocatorDefault, host as CFString, UInt32(port), &readStream, &writeStream)

        self.inputStream = readStream?.takeRetainedValue()
        self.outputStream = writeStream?.takeRetainedValue()

        guard let inputStream = self.inputStream, let outputStream = self.outputStream else {
            return
        }

        inputStream.delegate = self
        outputStream.delegate = self

        //CFReadStreamSetDispatchQueue(inputStream, streamQueue)
        //CFWriteStreamSetDispatchQueue(outputStream, streamQueue)

        inputStream.schedule(in: .main, forMode: .commonModes)
        outputStream.schedule(in: .main, forMode: .commonModes)

        inputStream.open()
        outputStream.open()
    }

    func disconnect() {
        guard let inputStream = inputStream, let outputStream = outputStream else {
            return
        }

        inputStream.delegate = nil
        outputStream.delegate = nil

        inputStream.close()
        outputStream.close()

        inputStream.remove(from: .main, forMode: .commonModes)
        outputStream.remove(from: .main, forMode: .commonModes)

        isConnected = false
    }

    func send(message: Any) {
        serialQueue.sync { [weak self] in
            self?.messageQueue.append(message)
        }

        writeToStream()
    }

    func write(raw: [UInt8]) {
        var data = raw
        DispatchQueue.global().async { [weak self] in
            if let outputStream = self?.outputStream, outputStream.write(&data, maxLength: data.count) == -1 {
                print("Failed write to output stream\n")
            }
        }
    }

    fileprivate func endEncountered(stream: Stream) {
        disconnect()
        delegate?.didDisconnect()
    }

    fileprivate func openCompleted(stream: Stream) {
        guard let inputStream = inputStream, let outputStream = outputStream else {
            return
        }

        if inputStream.streamStatus == .open, outputStream.streamStatus == .open {
            isConnected = true
            isTryingConnect = false
            delegate?.didConnect(stream: stream)
        }
    }

    fileprivate func handleIncommingBytes(_ stream: Stream) {
        if let stream = stream as? InputStream {
            DispatchQueue.global().async { [weak self] in
                self?.serializer.deserialize(stream: stream, onSuccess: { [weak self] message in
                    if let message = message {
                        self?.delegate?.didReceiveMessage(stream: stream, message: message)
                    }
                }, onError: { error in
                    print("Failed deserialize :: \(error.localizedDescription)")
                })
            }
        }
    }

    fileprivate func writeToStream() {
        guard !messageQueue.isEmpty, let outputStream = outputStream, outputStream.hasSpaceAvailable else {
            return
        }

        DispatchQueue.global().async { [weak self] in
            let message = self?.serialQueue.sync {
                return self?.messageQueue.removeLast()
            }

            self?.serializer.serialize(stream: outputStream, message: message, onSuccess: {
                print("Success Send message")
            }, onError: { [weak self] error in
                print("Failed serialize json to binary while send message. :: \(error.localizedDescription)")
                self?.serialQueue.sync {
                    if let message = message {
                        self?.messageQueue.append( message )
                    }
                }
            })
        }
    }

}

// MARK: - StreamDelegate
extension CommunicationLayer: StreamDelegate {

    func stream(_ aStream: Stream, handle eventCode: Stream.Event) {
        switch eventCode {
        case .endEncountered:
            break
        case .errorOccurred:
            isConnected = false
            isTryingConnect = false
            disconnect()
            print("Stream Error :: \(aStream.streamError?.localizedDescription ?? "")")
            break
        case .hasBytesAvailable:
            isTryingConnect = false
            handleIncommingBytes(aStream)
        case .hasSpaceAvailable:
            isTryingConnect = false
            writeToStream()
        case .openCompleted:
            openCompleted(stream: aStream)
        default:
            break
        }

        switch aStream.streamStatus {
        case .open:
            break
        case .notOpen:
            break
        case .opening:
            break
        case .reading:
            break
        case .writing:
            break
        case .atEnd:
            break
        case .closed:
            break
        case .error:
            break
        }
    }

}

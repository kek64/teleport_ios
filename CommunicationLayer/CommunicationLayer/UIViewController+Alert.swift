//
//  UIViewController+Alert.swift
//  Teleport
//
//  Created by VoidPtr on 22.02.18.
//  Copyright © 2018 HackGroup. All rights reserved.
//

import UIKit

extension UIViewController {

    func showMessage(_ title: String = "", _ message: String) {
        guard !message.isEmpty else {
            return
        }

        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let dismissAction = UIAlertAction(title: "OK", style: .default) { _ in

        }
        alertController.addAction(dismissAction)
        present(alertController, animated: true, completion: nil)
    }

}

//
//  UIImageView+Extensions.swift
//  Teleport
//
//  Created by VoidPtr on 3/20/18.
//  Copyright © 2018 VoidPtr. All rights reserved.
//

import UIKit

extension UIImageView {

    func download(from urlString: String, _ completion: @escaping (String?) -> Void = { _ in }) {
        guard let url = URL(string: urlString) else {
            completion(nil)
            return
        }

        download(from: url, completion)
    }

    func download(from url: URL, _ completion: @escaping (String?) -> Void = { _ in }) {
        UIImageView.download(from: url) { [weak self] downloadedImage, path in
            guard let strongSelf = self else {
                completion(nil)
                return
            }

            DispatchQueue.main.async {
                if let downloadedImage = downloadedImage {
                    strongSelf.image = downloadedImage
                }

                completion(path)
            }
        }
    }

    static func download(from url: URL, _ completion: @escaping (UIImage?, String?) -> Void = { _, _ in }) {
        let persistent = PersistentLayer()

        if let cachedImage = persistent.fetch(url: url) {
            completion(cachedImage, nil)
            return
        }

        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        URLSession.shared.dataTask(with: url) { data, response, error in
            DispatchQueue.main.async {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
            }

            guard error == nil, let mimeType = response?.mimeType, mimeType.hasPrefix("image") else {
                completion(nil, nil)
                return
            }

            if let data = data {
                DispatchQueue.main.async {
                    if let downloadedImage = UIImage(data: data) {
                        let path = persistent.store(imageData: data, url: url)
                        completion(downloadedImage, path)
                    }
                }
            }
            }.resume()
    }

}

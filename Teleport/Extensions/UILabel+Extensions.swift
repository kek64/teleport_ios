//
//  UILabel+Extensions.swift
//  Teleport
//
//  Created by VoidPtr on 3/12/18.
//  Copyright © 2018 HackGroup. All rights reserved.
//

import UIKit

extension UILabel {

    static func defaultLabel(fontName: String, size: CGFloat, color: UIColor = .white) -> UILabel {
        let label = UILabel()
        if let prefferedFont = UIFont(name: fontName, size: size) {
            label.font = prefferedFont
        }
        label.textColor = color
        return label
    }

    static func defaultLabel(with size: CGFloat, color: UIColor = .white) -> UILabel {
        return defaultLabel(fontName: "AvenirNext", size: size, color: color)
    }

    static func defaultBoldLabel(with size: CGFloat, color: UIColor = .white) -> UILabel {
        return defaultLabel(fontName: "AvenirNext-Bold", size: size, color: color)
    }

    static func defaultMediumLabel(with size: CGFloat, color: UIColor = .white) -> UILabel {
        return defaultLabel(fontName: "AvenirNext-Medium", size: size, color: color)
    }

}

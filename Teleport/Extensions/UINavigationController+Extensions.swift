//
//  UINavigationController+Extensions.swift
//  Teleport
//
//  Created by VoidPtr on 3/20/18.
//  Copyright © 2018 VoidPtr. All rights reserved.
//

import UIKit

extension UINavigationController {

    func pushAndHideBottomBar(_ viewController: UIViewController, animated: Bool) {
        viewController.hidesBottomBarWhenPushed = true
        viewController.navigationItem.largeTitleDisplayMode = .never
        pushViewController(viewController, animated: animated)
    }

}

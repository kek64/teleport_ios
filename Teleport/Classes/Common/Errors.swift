//
//  Errors.swift
//  Teleport
//
//  Created by VoidPtr on 3/21/18.
//  Copyright © 2018 VoidPtr. All rights reserved.
//

import Foundation

enum AppError: Error {
    case unexpected(errorString: String)
    case success
    case anauthorized
    case badRequest
    case failedFetchContacts
    case failedFetchContactDetails
    case noContactsFound
}

extension AppError: LocalizedError {

    public var errorDescription: String? {
        switch self {
        case .unexpected(let errorString):
            return NSLocalizedString(errorString, comment: "")
        case .success:
            return NSLocalizedString("Success.", comment: "")
        case .anauthorized:
            return NSLocalizedString("Anauthorized.", comment: "")
        case .badRequest:
            return NSLocalizedString("Something went wrong.", comment: "")
        case .failedFetchContacts:
            return NSLocalizedString("Unable to fetch contacts.", comment: "")
        case .failedFetchContactDetails:
            return NSLocalizedString("Unable to fetch contact details.", comment: "")
        case .noContactsFound:
            return NSLocalizedString("There are no contacts yet.", comment: "")
        }
    }
}

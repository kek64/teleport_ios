//
//  Constants.swift
//  Teleport
//
//  Created by VoidPtr on 3/22/18.
//  Copyright © 2018 VoidPtr. All rights reserved.
//

import UIKit

public struct AppConstants {

    public struct Colors {
        static let cardAlpha: CGFloat = 0.4
        static let cardColor1 = UIColor(red: 162.0/255.0, green: 155.0/255.0, blue: 254.1/255.0, alpha: cardAlpha)
        static let cardColor2 = UIColor(red: 255/255.0, green: 234/255.0, blue: 167/255.0, alpha: cardAlpha)
        static let cardColor3 = UIColor(red: 129/255.0, green: 236/255.0, blue: 236/255.0, alpha: cardAlpha)
        static let cardColor4 = UIColor(red: 85/255.0, green: 239/255.0, blue: 196/255.0, alpha: cardAlpha)
        static let cardColor5 = UIColor(red: 178/255.0, green: 190/255.0, blue: 195/255.0, alpha: cardAlpha)
        static let cardColor6 = UIColor(red: 225/255.0, green: 112/255.0, blue: 85/255.0, alpha: cardAlpha)
        static let cardColor7 = UIColor(red: 45/255.0, green: 52/255.0, blue: 54/255.0, alpha: cardAlpha)
        static let cardColor8 = UIColor(red: 9/255.0, green: 132/255.0, blue: 227/255.0, alpha: cardAlpha)
        static let allValues = [cardColor1, cardColor2, cardColor3, cardColor4, cardColor5, cardColor6, cardColor7, cardColor8]
    }

}

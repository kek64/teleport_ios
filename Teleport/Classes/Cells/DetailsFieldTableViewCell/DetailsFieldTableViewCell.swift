//
//  DetailsFieldTableViewCell.swift
//  Teleport
//
//  Created by VoidPtr on 3/22/18.
//  Copyright © 2018 VoidPtr. All rights reserved.
//

import UIKit

final class DetailsFieldTableViewCell: GenericTableViewCell<User> {

    // MARK: - Properties

    override var data: User? {
        didSet {
            configurate()
        }
    }

    // MARK: - Life-Cycle

    override func prepareForReuse() {
        super.prepareForReuse()
    }

    // MARK: - Configurations

    override func configurate() {
        super.configurate()

    }

}

//
//  GenericTableViewCell.swift
//  Teleport
//
//  Created by VoidPtr on 3/20/18.
//  Copyright © 2018 VoidPtr. All rights reserved.
//

import UIKit

class GenericTableViewCell<T>: UITableViewCell {

    open var data: T?

    // MARK: - Life-Cycle

    override func awakeFromNib() {
        super.awakeFromNib()
        configurate()
    }

    // MARK: - Configurations

    func configurate() {
        selectionStyle = .none
    }

}

//
//  ChatTableViewCell.swift
//  Teleport
//
//  Created by VoidPtr on 3/20/18.
//  Copyright © 2018 VoidPtr. All rights reserved.
//

import UIKit

final class ChatTableViewCell: GenericTableViewCell<ChatViewModel> {

    // MARK: - Properties

    static let identifier = String(describing: ChatTableViewCell.self)
    
    override var data: ChatViewModel? {
        didSet {
            configurate()
        }
    }

    private struct Constants {
        static let avatarScale: CGFloat = 68.0
        static let avatarCorners: CGFloat = 12.0 // 34.0
        static let cardCorners: CGFloat = 10.0
        static let topOffset: CGFloat = 4.0
        static let bottomOffset: CGFloat = 5.0
        static let sideOffsets: CGFloat = 6.0
    }

    public lazy var containerView: UIView = {
        let containerView = UIView()
        containerView.backgroundColor = UIColor(white: 0.3, alpha: 0.8)
        containerView.layer.masksToBounds = true
        addSubview(containerView)
        containerView.anchor(topAnchor,
                             left: leftAnchor,
                             bottom: bottomAnchor,
                             right: rightAnchor,
                             topConstant: Constants.topOffset,
                             leftConstant: Constants.sideOffsets,
                             bottomConstant: Constants.bottomOffset,
                             rightConstant: Constants.sideOffsets)
        return containerView
    }()

    private lazy var contactAvatarImageView: UIImageView = {
        let contactAvatarImageView = UIImageView()
        contactAvatarImageView.contentMode = .scaleAspectFill
        contactAvatarImageView.layer.masksToBounds = true
        containerView.addSubview(contactAvatarImageView)
        contactAvatarImageView.anchor(containerView.topAnchor,
                                      left: containerView.leftAnchor,
                                      bottom: containerView.bottomAnchor,
                                      topConstant: 8.0,
                                      leftConstant: 8.0,
                                      bottomConstant: 8.0,
                                      widthConstant: Constants.avatarScale,
                                      heightConstant: Constants.avatarScale)
        return contactAvatarImageView
    }()

    private lazy var nameLabel: UILabel = {
        let nameLabel = UILabel.defaultMediumLabel(with: 16.0)
        containerView.addSubview(nameLabel)
        nameLabel.anchor(contactAvatarImageView.topAnchor,
                         left: contactAvatarImageView.rightAnchor,
                         right: containerView.rightAnchor,
                         topConstant: 2.0,
                         leftConstant: 8.0,
                         rightConstant: 8.0)
        return nameLabel
    }()

    private lazy var messageBodyLabel: UILabel = {
        let messageBodyLabel = UILabel.defaultMediumLabel(with: 14.0, color: .lightGray)
        containerView.addSubview(messageBodyLabel)
        messageBodyLabel.anchor(nameLabel.bottomAnchor,
                           left: nameLabel.leftAnchor,
                           right: nameLabel.rightAnchor,
                           leftConstant: 0.0)
        return messageBodyLabel
    }()

    // MARK: - Life-Cycle

    override func layoutSubviews() {
        super.layoutSubviews()
        contactAvatarImageView.layer.cornerRadius = Constants.avatarCorners
        containerView.layer.cornerRadius = Constants.cardCorners
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        nameLabel.text = nil
        messageBodyLabel.text = nil
        contactAvatarImageView.image = nil
    }

    // MARK: - Configurations

    override func configurate() {
        super.configurate()
        configureShadows()

        if let url = data?.avatarURL {
            contactAvatarImageView.download(from: url)
        } else {
            contactAvatarImageView.image = #imageLiteral(resourceName: "male-user")
        }

        nameLabel.text = data?.name ?? "Unknown"
        messageBodyLabel.text = data?.body ?? ""
    }

    private func configureShadows() {
        backgroundColor = .black
        containerView.layer.shadowOpacity = 0.28
        containerView.layer.shadowOffset = CGSize(width: 0, height: 2)
        containerView.layer.shadowRadius = 2
        containerView.layer.shadowColor = UIColor.darkGray.cgColor
        containerView.layer.masksToBounds = false
    }

}

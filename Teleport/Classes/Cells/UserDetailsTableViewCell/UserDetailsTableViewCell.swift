//
//  UserDetailsTableViewCell.swift
//  Teleport
//
//  Created by VoidPtr on 3/20/18.
//  Copyright © 2018 VoidPtr. All rights reserved.
//

import UIKit

final class UserDetailsTableViewCell: GenericTableViewCell<UserDetailsBaseViewModel> {
    
    // MARK: - Properties

    private struct Constants {
        static let avatarScale: CGFloat = 48.0
        static let avatarCorners: CGFloat = 10.0 // 34.0
        static let cardCorners: CGFloat = 10.0
        static let topOffset: CGFloat = 2.0
        static let bottomOffset: CGFloat = 5.0
        static let sideOffsets: CGFloat = 4.0
    }

    static let identifier = String(describing: UserDetailsTableViewCell.self)

    override var data: UserDetailsBaseViewModel? {
        didSet {
            configurate()
        }
    }

    public lazy var containerView: UIView = {
        let containerView = UIView()
        containerView.backgroundColor = UIColor(white: 0.3, alpha: 0.8)
        containerView.layer.masksToBounds = true
        addSubview(containerView)
        containerView.anchor(topAnchor,
                             left: leftAnchor,
                             bottom: bottomAnchor,
                             right: rightAnchor,
                             topConstant: Constants.topOffset,
                             leftConstant: Constants.sideOffsets,
                             bottomConstant: Constants.bottomOffset,
                             rightConstant: Constants.sideOffsets)
        return containerView
    }()

    private lazy var detailTitleLabel: UILabel = {
        let nameLabel = UILabel.defaultMediumLabel(with: 16.0)
        containerView.addSubview(nameLabel)
        nameLabel.anchor(containerView.topAnchor,
                         left: containerView.rightAnchor,
                         right: containerView.rightAnchor,
                         topConstant: 6.0,
                         leftConstant: 8.0,
                         rightConstant: 8.0)
        return nameLabel
    }()

    private lazy var detailLabel: UILabel = {
        let detailLabel = UILabel.defaultMediumLabel(with: 14.0, color: .lightGray)
        containerView.addSubview(detailLabel)
        detailLabel.anchor(detailTitleLabel.bottomAnchor,
                           left: detailTitleLabel.leftAnchor,
                           bottom: containerView.bottomAnchor,
                           right: detailTitleLabel.rightAnchor,
                           leftConstant: 1.0,
                           bottomConstant: 8.0)
        return detailLabel
    }()

    // MARK: - Life-Cycle

    override func prepareForReuse() {
        super.prepareForReuse()

    }

    // MARK: - Configurations

    override func configurate() {
        super.configurate()
        configureShadows()

        if let details = data {
            detailTitleLabel.text = details.title
            detailLabel.text = details.subtitle
        }
    }

    private func configureShadows() {
        backgroundColor = .black
        containerView.layer.shadowOpacity = 0.28
        containerView.layer.shadowOffset = CGSize(width: 0, height: 2)
        containerView.layer.shadowRadius = 2
        containerView.layer.shadowColor = UIColor.darkGray.cgColor
        containerView.layer.masksToBounds = false
    }

}

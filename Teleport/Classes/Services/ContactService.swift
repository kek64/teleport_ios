//
//  ContactService.swift
//  Teleport
//
//  Created by VoidPtr on 3/20/18.
//  Copyright © 2018 VoidPtr. All rights reserved.
//

import Contacts
import CoreData

final class ContactsService {

    // MARK: - Properties

    fileprivate let contactStore: CNContactStore

    // MARK: - Life-Cycle

    init() {
        self.contactStore = CNContactStore()
    }

    // MARK: - Public

    func fetchDetails(contactId: String, _ completion: @escaping (ResponseData<ContactViewModel>) -> Void) {
        fetchDeviceContacts { (contactsResponse: ResponseData<[ContactViewModel]>) in
            switch contactsResponse {
            case .success(let contacts):
                if let contact = contacts.first(where: { $0.user.userId == contactId }) {
                    completion(.success(contact))
                } else {
                    completion(.failure(.failedFetchContactDetails))
                }
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }

    func fetchDeviceContacts(_ completion: @escaping (ResponseData<[ContactViewModel]>) -> Void) {
        requestAccess { [weak self] isSuccess, error in
            guard let strongSelf = self else {
                return
            }

            if isSuccess {
                guard let keys = [CNContactFormatter.descriptorForRequiredKeys(for: .fullName),
                                  CNContactEmailAddressesKey,
                                  CNContactPhoneNumbersKey,
                                  CNContactImageDataAvailableKey,
                                  CNContactThumbnailImageDataKey] as? [CNKeyDescriptor] else {
                    completion(.failure(.failedFetchContacts))
                    return
                }

                do {
                    let contactContainers = try strongSelf.contactStore.containers(matching: nil)
                    var fetchedContacts: [CNContact] = []

                    for container in contactContainers {
                        let fetchPredicate = CNContact.predicateForContactsInContainer(withIdentifier: container.identifier)
                        let containerResults = try strongSelf.contactStore.unifiedContacts(matching: fetchPredicate, keysToFetch: keys)
                        fetchedContacts += containerResults
                    }

                    if fetchedContacts.isEmpty {
                        completion(.failure(.noContactsFound))
                    }

                    var index = 0
                    let contacts: [ContactViewModel] = fetchedContacts.map({ (contact: CNContact) in
                        let userName = "\(contact.givenName) \(contact.familyName)"
                        let aliasName = contact.nickname
                        let phone = contact.phoneNumbers.isEmpty ? nil : contact.phoneNumbers[0].value.stringValue
                        let email = contact.emailAddresses.isEmpty ? nil : contact.emailAddresses[0].value as String
                        let avatar = contact.thumbnailImageData
                        let contactId = String(describing: index)

                        index += 1
                        return ContactViewModel(user: UserViewModel(userId: contactId,
                                                 fullName: userName,
                                                 aliasName: aliasName,
                                                 phone: phone,
                                                 email: email,
                                                 avatarData: avatar))
                    })

                    DispatchQueue.main.async {
                        completion(.success(contacts))
                    }

                } catch {
                    DispatchQueue.main.async {
                        completion(.failure(.failedFetchContacts))
                    }
                }
            } else {
                completion(.failure(.noContactsFound))
            }
        }
    }

    // MARK: - Private

    private func requestAccess(_ completion: @escaping (_ isSuccess: Bool, _ error: String?) -> Void) {
        let authStatus = CNContactStore.authorizationStatus(for: .contacts)
        let errorMessage = "Failed access to contacts book."

        switch authStatus {
        case .authorized:
            completion(true, nil)
        case .denied, .notDetermined:
            self.contactStore.requestAccess(for: .contacts, completionHandler: { (isSuccess, error) in
                if isSuccess {
                    completion(true, nil)
                } else {
                    if authStatus == CNAuthorizationStatus.denied {
                        DispatchQueue.main.async {
                            let message = "\(error!.localizedDescription)\n"
                            completion(false, message)
                        }
                    }
                }
            })
        default:
            completion(false, errorMessage)
        }
    }

}

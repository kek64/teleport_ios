//
//  AppConfiguratorService.swift
//  Teleport
//
//  Created by VoidPtr on 3/20/18.
//  Copyright © 2018 VoidPtr. All rights reserved.
//

import UIKit

final class AppConfiguratorService {

    // MARK: - Public

    func configurateApplication() -> UIViewController {
        let keychain = KeychainLayer()
        configurateAppAppearance()

        if keychain.sessionToken != nil {
            return configurateTabBar()
        } else {
            return configurateStartupViewController()
        }
    }

    // MARK: - Private

    private func configurateAppAppearance() {
        UIApplication.shared.statusBarStyle = .lightContent
        UINavigationBar.appearance().tintColor = .groupTableViewBackground
        UINavigationBar.appearance().barStyle = .black
        UINavigationBar.appearance().largeTitleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white,
                                                                 NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 22.0)]

        UITabBar.appearance().tintColor = .groupTableViewBackground
        UITabBar.appearance().barStyle = .black

        let backgroundView = UIView()
        backgroundView.backgroundColor = .darkGray
        UITableViewCell.appearance().selectedBackgroundView = backgroundView
    }

    private func configurateStartupViewController() -> UIViewController {
        let navigationController = UINavigationController(rootViewController: SplashViewController())
        navigationController.hidesBarsWhenKeyboardAppears = false
        return navigationController
    }

    private func configurateTabBar() -> UITabBarController {
        let tabBarVC = UITabBarController()

        let contactsVC = ContactsListViewController()
        let channelsVC = ChannelsListViewController()
        let chatListVC = ChatsListViewController()
        let profileVC = ProfileViewController()

        ContactsListModuleConfigurator().configureModuleForViewInput(viewInput: contactsVC)
        ChannelsListModuleConfigurator().configureModuleForViewInput(viewInput: channelsVC)
        ChatsListModuleConfigurator().configureModuleForViewInput(viewInput: chatListVC)
        ProfileModuleConfigurator().configureModuleForViewInput(viewInput: profileVC)

        contactsVC.tabBarItem = UITabBarItem(title: "Contacts", image: #imageLiteral(resourceName: "ModernContactListCreateGroupIcon"), tag: 0)
        channelsVC.tabBarItem = UITabBarItem(title: "Channels", image: #imageLiteral(resourceName: "ModernContactListBroadcastIcon"), tag: 1)
        chatListVC.tabBarItem = UITabBarItem(title: "Chats", image: #imageLiteral(resourceName: "ModernContactListCreateSecretChatIcon"), tag: 2)
        profileVC.tabBarItem = UITabBarItem(title: "Settings", image: #imageLiteral(resourceName: "icSettings"), tag: 3)

        tabBarVC.tabBar.isTranslucent = false
        tabBarVC.viewControllers = [UINavigationController(rootViewController: contactsVC),
                                    UINavigationController(rootViewController: channelsVC),
                                    UINavigationController(rootViewController: chatListVC),
                                    UINavigationController(rootViewController: profileVC)]

        if let viewControllers = tabBarVC.viewControllers {
            for navigationController in viewControllers {
                if let navigationController = navigationController as? UINavigationController {
                    navigationController.navigationBar.prefersLargeTitles = true
                    navigationController.navigationBar.isTranslucent = false
                    navigationController.hidesBarsWhenKeyboardAppears = false
                }
            }
        }

        return tabBarVC
    }
    
}

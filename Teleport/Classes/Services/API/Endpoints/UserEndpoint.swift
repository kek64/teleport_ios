//
//  UserEndpoint.swift
//  Teleport
//
//  Created by VoidPtr on 22.02.18.
//  Copyright © 2018 HackGroup. All rights reserved.
//

import Foundation

enum UserEndpoint {
    case validatePhone(phoneNumber: String)
    case register(user: User)
    case login(phone: String, password: String)
    case update(user: User)
    case delete(phone: String)
    case logout()
    case profile()
    case synchronize(phoneNumbers: [String])
    case search(alias: String)
}

extension UserEndpoint: EndpointProtocol {

    var parameters: [String: Any]? {
        switch self {
        case .validatePhone(let phoneNumber):
            return ["phone": phoneNumber]
        case .register(let user):
            return [User.CodingKeys.userName.rawValue: user.userName ?? "",
                    User.CodingKeys.alias.rawValue: user.alias ?? "",
                    User.CodingKeys.email.rawValue: user.email ?? "",
                    User.CodingKeys.phone.rawValue: user.phone ?? "",
                    "password": user.password,
                    User.CodingKeys.details.rawValue: user.details ?? ""
            ]
        case .login(let phone, let password):
            return ["phone": phone,
                    "password": password
            ]
        case .update(let user):
            return [User.CodingKeys.userName.rawValue: user.userName ?? "",
                    User.CodingKeys.alias.rawValue: user.alias ?? "",
                    User.CodingKeys.email.rawValue: user.email ?? "",
                    User.CodingKeys.phone.rawValue: user.phone ?? "",
                    "password": user.password,
                    User.CodingKeys.details.rawValue: user.details ?? ""
            ]
        case .delete(let phone):
            return ["phone": phone]
        case .logout, .profile, .search:
            return nil
        case .synchronize(let phoneNumbers):
            return ["phoneNumbers": phoneNumbers]
        }
    }

    var multipartParameters: [String: Any]? {
        return nil
    }

    var multipartFormat: [String: String]? {
        return nil
    }

    var path: String {
        switch self {
        case .validatePhone:
            return "/users/validate-phone"
        case .delete:
            return "/users/delete"
        case .login:
            return "/users/login"
        case .register:
            return "/users/register"
        case .update:
            return "/users/update"
        case .logout:
            return "/users/terminate-sessions"
        case .profile:
            return "/users/profile"
        case .synchronize:
            return "/users/synchronize"
        case .search(let alias):
            return "/users/search&alias=\(alias)"
        }
    }

    var method: HTTPMethod {
        switch self {
        case .validatePhone:
            return .post
        case .delete:
            return .delete
        case .login:
            return .post
        case .register:
            return .post
        case .update:
            return .put
        case .logout:
            return .post
        case .profile:
            return .get
        case .synchronize:
            return .post
        case .search:
            return .get
        }
    }

}

//
//  ChatEndpoint.swift
//  Teleport
//
//  Created by VoidPtr on 4/8/18.
//  Copyright © 2018 VoidPtr. All rights reserved.
//

import Foundation

enum ChatEndpoint {
    case all
    case create(chat: Chat)
    case rename(chatId: Int, name: String)
    case delete(chatId: Int)
}

extension ChatEndpoint: EndpointProtocol {

    var parameters: [String: Any]? {
        switch self {
        case .all:
            return nil
        case .create(let chat):
            return [Chat.CodingKeys.name.rawValue: chat.name ?? "",
                    Chat.CodingKeys.invitedId.rawValue: chat.invitedId]
        case .rename(let chatId, let name):
            return [Chat.CodingKeys.id.rawValue: chatId,
                    Chat.CodingKeys.name.rawValue: name ]
        case .delete(let chatId):
            return [Chat.CodingKeys.id.rawValue: chatId]
        }
    }

    var path: String {
        switch self {
        case .all:
            return "/chats/all"
        case .create:
            return "/chats/create"
        case .rename:
            return "/chats/rename"
        case .delete:
            return "/chats/delete"
        }
    }

    var method: HTTPMethod {
        switch self {
        case .all:
            return .get
        case .create:
            return .post
        case .rename:
            return .put
        case .delete:
            return .delete
        }
    }

    var multipartParameters: [String: Any]? {
        return nil
    }

    var multipartFormat: [String: String]? {
        return nil
    }

}

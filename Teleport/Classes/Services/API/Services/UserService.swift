//
//  UserService.swift
//  Teleport
//
//  Created by VoidPtr on 3/20/18.
//  Copyright © 2018 VoidPtr. All rights reserved.
//

import Foundation

final class UserService {

    // MARK: - Properties

    fileprivate let keychains: KeychainLayer
    fileprivate let networkManager: NetworkManager

    // MARK: - Life-Cycle

    init() {
        self.keychains = KeychainLayer()
        self.networkManager = NetworkManager(sessionToken: keychains.sessionToken)
    }

    // MARK: - Public

    func validatePhone(phoneNumber: String, _ completion: @escaping (ResponseData<ResponseMessage>) -> Void) {
        let endpoint = UserEndpoint.validatePhone(phoneNumber: phoneNumber)
        networkManager.performRequest(endpoint: endpoint, completion)
    }

    func register(user: User, _ completion: @escaping (ResponseData<AuthResponse>) -> Void) {
        let endpoint = UserEndpoint.register(user: user)
        networkManager.performRequest(endpoint: endpoint) { (response: ResponseData<AuthResponse>) in
            switch response {
            case .success(let result):
                // store cookie to keychains
                break
            case .failure(let error):
                break
            }
        }
    }

    func login(phone: String, password: String) {
        let endpoint = UserEndpoint.login(phone: phone, password: password)
        networkManager.performRequest(endpoint: endpoint) { (response: ResponseData<AuthResponse>) in
            switch response {
            case .success(let result):
                // store cookie to keychains
                break
            case .failure(let error):
                break
            }
        }
    }

    func update(user: User, _ completion: @escaping (ResponseData<User>) -> Void) {
        let endpoint = UserEndpoint.update(user: user)
        networkManager.performRequest(endpoint: endpoint, completion)
    }

    func delete(phone: String, _ completion: @escaping () -> Void) {
        let endpoint = UserEndpoint.delete(phone: phone)
        networkManager.performRequest(endpoint: endpoint) { (response: ResponseData<ResponseMessage>) in
            completion()
        }
    }

    func logout(_ completion: @escaping () -> Void) {
        let endpoint = UserEndpoint.logout()
        networkManager.performRequest(endpoint: endpoint) { (response: ResponseData<ResponseMessage>) in
            completion()
        }
    }

    func profile(_ completion: @escaping (ResponseData<User>) -> Void) {
        let endpoint = UserEndpoint.profile()
        networkManager.performRequest(endpoint: endpoint, completion)
    }

    func synchronize(phoneNumbers: [String], _ completion: @escaping (ResponseData<[User]>) -> Void) {
        let endpont = UserEndpoint.synchronize(phoneNumbers: phoneNumbers)
        networkManager.performRequest(endpoint: endpont, completion)
    }

    func search(alias: String, _ completion: @escaping (ResponseData<[User]>) -> Void) {
        let endpoint = UserEndpoint.search(alias: alias)
        networkManager.performRequest(endpoint: endpoint, completion)
    }
    
}

//
//  ChatService.swift
//  Teleport
//
//  Created by VoidPtr on 3/20/18.
//  Copyright © 2018 VoidPtr. All rights reserved.
//

import Foundation

final class ChatService {

    // MARK: - Properties

    fileprivate let keychainLayer: KeychainLayer
    fileprivate let networkManager: NetworkManager

    // MARK: - Life-Cycle

    init() {
        self.keychainLayer = KeychainLayer()
        self.networkManager = NetworkManager(sessionToken: keychainLayer.sessionToken)
    }

    // MARK: - Public

    func fetchChats(_ completion: @escaping (ResponseData<[Chat]>) -> Void) {
        let endpoint = ChatEndpoint.all
        networkManager.performRequest(endpoint: endpoint, completion)
    }

    func create(chat: Chat, _ completion: @escaping (ResponseData<Chat>) -> Void) {
        let endpoint = ChatEndpoint.create(chat: chat)
        networkManager.performRequest(endpoint: endpoint, completion)
    }

    func rename(chatId: Int, name: String, _ completion: @escaping (ResponseData<Chat>) -> Void) {
        let endpoint = ChatEndpoint.rename(chatId: chatId, name: name)
        networkManager.performRequest(endpoint: endpoint, completion)
    }

    func delete(chatId: Int, _ completion: @escaping () -> Void) {
        let endpoint = ChatEndpoint.delete(chatId: chatId)
        networkManager.performRequest(endpoint: endpoint) { (_: ResponseData<ResponseMessage>) in
            completion()
        }
    }
    
}

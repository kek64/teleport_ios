//
//  CommunicationService.swift
//  Teleport
//
//  Created by VoidPtr on 3/23/18.
//  Copyright © 2018 VoidPtr. All rights reserved.
//

import Foundation
import UIKit
import CFNetwork

public enum CommunicationServiceReconnectionPolicy {
    case always
    case never
    case periodicaly(fireTime: TimeInterval)
}

final class CommunicationService {

    // MARK: - Properties

    private struct Constats {
        static let host = "localhost"
        static let port = 13370
    }

    private let communicationLayer: CommunicationLayer
    private let reconnectionPolicy: CommunicationServiceReconnectionPolicy
    private var reconnectTimer: Timer!

    // MARK: - Life-Cycle

    init(reconnectionPolicy: CommunicationServiceReconnectionPolicy) {
        self.reconnectionPolicy = reconnectionPolicy
        self.communicationLayer = CommunicationLayer(host: Constats.host,
                                                     port: Constats.port,
                                                     serializer: CommunicationLayerJSONSerializer())
        self.communicationLayer.delegate = self
        self.configureTimer()
    }

    // MARK: - Private

    private func configureTimer() {
        switch reconnectionPolicy {
        case .always, .never:
            break
        case .periodicaly(let fireTime):
            reconnectTimer = Timer(timeInterval: fireTime, target: self, selector: #selector(reconnectIfNeeded(_:)), userInfo: nil, repeats: true)
            RunLoop.main.add(reconnectTimer, forMode: .commonModes)
        }
    }

    @objc
    private func reconnectIfNeeded(_ timer: Timer?) {
        if !communicationLayer.isConnected && !communicationLayer.isTryingConnect {
            print("[Timer] - Try reconnect")
            communicationLayer.establishConnect()
        }
    }


    // MARK: - Public

    func send(message: String) {
        switch reconnectionPolicy {
        case .always:
            reconnectIfNeeded(nil)
        default:
            break
        }

        let messageParams = ["senderId": 12,
                             "receiverId": 13,
                             "body": message] as [String : Any]
        communicationLayer.send(message: messageParams)
    }

}

// MARK: - CommunicationLayerDelegate
extension CommunicationService: CommunicationLayerDelegate {

    func didConnect(stream: Stream) {
        print("Success conntected")
    }

    func didDisconnet(stream: Stream, message: String) {
        switch reconnectionPolicy {
        case .always:
            reconnectIfNeeded(nil)
        default:
            break
        }

        print("Disconnected with message :: \(message)")
    }

    func didReceiveMessage(stream: Stream, message: [String: Any]) {
        DispatchQueue.main.async {
            if let delegate = UIApplication.shared.delegate as? AppDelegate {
                if let controller = delegate.window?.rootViewController {
                    controller.showMessage("Receive message", message.description)
                }
            }
        }
        print("Receive message :: \(message)")
    }

    func didDisconnect() {
        print("Disconnect")
    }

}

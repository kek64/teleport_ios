//
//  ChannelsDetailsChannelsDetailsConfigurator.swift
//  Teleport
//
//  Created by VoidPtr on 20/03/2018.
//  Copyright © 2018 kekussoft. All rights reserved.
//

import UIKit

class ChannelsDetailsModuleConfigurator {

    func configureModuleForViewInput<UIViewController>(viewInput: UIViewController) {

        if let viewController = viewInput as? ChannelsDetailsViewController {
            configure(viewController: viewController)
        }
    }

    private func configure(viewController: ChannelsDetailsViewController) {

        let router = ChannelsDetailsRouter()

        let presenter = ChannelsDetailsPresenter()
        presenter.view = viewController
        presenter.router = router

        let interactor = ChannelsDetailsInteractor()
        interactor.output = presenter

        presenter.interactor = interactor
        viewController.output = presenter
    }

}

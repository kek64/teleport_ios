//
//  ChannelsDetailsChannelsDetailsPresenter.swift
//  Teleport
//
//  Created by VoidPtr on 20/03/2018.
//  Copyright © 2018 kekussoft. All rights reserved.
//

class ChannelsDetailsPresenter: ChannelsDetailsModuleInput, ChannelsDetailsViewOutput, ChannelsDetailsInteractorOutput {

    weak var view: ChannelsDetailsViewInput!
    var interactor: ChannelsDetailsInteractorInput!
    var router: ChannelsDetailsRouterInput!

    func viewIsReady() {

    }
}

//
//  ContactDetailsContactDetailsViewController.swift
//  Teleport
//
//  Created by VoidPtr on 20/03/2018.
//  Copyright © 2018 kekussoft. All rights reserved.
//

import UIKit

final class ContactDetailsViewController: UIViewController, ContactDetailsViewInput {

    // MARK: - Properties

    var output: ContactDetailsViewOutput!

    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.backgroundColor = .black
        tableView.backgroundView = UIView()
        tableView.backgroundView?.backgroundColor = .black
        tableView.separatorStyle = .none
        tableView.dataSource = self
        tableView.delegate = self
        view.addSubview(tableView)
        tableView.fillSuperview()
        return tableView
    }()

    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        output.viewIsReady()
    }


    // MARK: ContactDetailsViewInput
    func setupInitialState() {
        tableView.register(UserDetailsTableViewCell.self, forCellReuseIdentifier: UserDetailsTableViewCell.identifier)
        output.fetchContactDetails()
    }

    func setTitle(_ title: String) {
        self.title = title
    }

    func showError(_ error: String) {
        showMessage("Error", error)
    }

}

// MARK: - UITableViewDataSource, UITableViewDelegate
extension ContactDetailsViewController: UITableViewDataSource, UITableViewDelegate {

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: UserDetailsTableViewCell.identifier, for: indexPath) as? UserDetailsTableViewCell else {
            return UITableViewCell()
        }

        return cell
    }

}

//
//  ContactDetailsContactDetailsConfigurator.swift
//  Teleport
//
//  Created by VoidPtr on 20/03/2018.
//  Copyright © 2018 kekussoft. All rights reserved.
//

import UIKit

class ContactDetailsModuleConfigurator {

    func configureModuleForViewInput<UIViewController>(viewInput: UIViewController, contactId: String) {

        if let viewController = viewInput as? ContactDetailsViewController {
            configure(viewController: viewController, contactId: contactId)
        }
    }

    private func configure(viewController: ContactDetailsViewController, contactId: String) {

        let router = ContactDetailsRouter()

        let presenter = ContactDetailsPresenter()
        presenter.view = viewController
        presenter.router = router
        presenter.store(contactId: contactId)

        let contactsService = ContactsService()
        let interactor = ContactDetailsInteractor(contactsService: contactsService)
        interactor.output = presenter

        presenter.interactor = interactor
        viewController.output = presenter
    }

}

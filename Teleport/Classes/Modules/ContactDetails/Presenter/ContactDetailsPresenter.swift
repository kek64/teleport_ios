//
//  ContactDetailsContactDetailsPresenter.swift
//  Teleport
//
//  Created by VoidPtr on 20/03/2018.
//  Copyright © 2018 kekussoft. All rights reserved.
//

class ContactDetailsPresenter: ContactDetailsModuleInput, ContactDetailsViewOutput, ContactDetailsInteractorOutput {

    weak var view: ContactDetailsViewInput!
    var interactor: ContactDetailsInteractorInput!
    var router: ContactDetailsRouterInput!

    private var contactId: String!
    private var contact: ContactViewModel!

    func viewIsReady() {
        view.setupInitialState()
    }

    func store(contactId: String) {
        self.contactId = contactId
    }

    func fetchContactDetails() {
        interactor.fetchProfileDetails(contactId: contactId)
    }

    func showError(_ error: String) {
        view.showError(error)
    }

    func contactFetched(_ contact: ContactViewModel) {
        self.contact = contact
        view.setTitle(contact.user.fullName)
    }

}

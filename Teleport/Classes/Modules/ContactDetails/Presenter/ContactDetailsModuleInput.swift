//
//  ContactDetailsContactDetailsModuleInput.swift
//  Teleport
//
//  Created by VoidPtr on 20/03/2018.
//  Copyright © 2018 kekussoft. All rights reserved.
//

protocol ContactDetailsModuleInput: class {
    func store(contactId: String)
}

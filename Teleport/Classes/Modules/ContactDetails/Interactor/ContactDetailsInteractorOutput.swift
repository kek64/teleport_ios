//
//  ContactDetailsContactDetailsInteractorOutput.swift
//  Teleport
//
//  Created by VoidPtr on 20/03/2018.
//  Copyright © 2018 kekussoft. All rights reserved.
//

import Foundation

protocol ContactDetailsInteractorOutput: class {
    func showError(_ error: String)
    func contactFetched(_ contact: ContactViewModel)
}

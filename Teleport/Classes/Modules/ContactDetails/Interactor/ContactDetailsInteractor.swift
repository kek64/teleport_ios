//
//  ContactDetailsContactDetailsInteractor.swift
//  Teleport
//
//  Created by VoidPtr on 20/03/2018.
//  Copyright © 2018 kekussoft. All rights reserved.
//

class ContactDetailsInteractor: ContactDetailsInteractorInput {

    weak var output: ContactDetailsInteractorOutput!
    private let contactsService: ContactsService

    init(contactsService: ContactsService) {
        self.contactsService = contactsService
    }

    func fetchProfileDetails(contactId: String) {
        contactsService.fetchDetails(contactId: contactId) { [weak self] response in
            guard let strongSelf = self else {
                return
            }

            switch response {
            case .success(let contact):
                strongSelf.output.contactFetched(contact)
            case .failure(let error):
                strongSelf.output.showError(error.errorDescription!)
            }
        }
    }

}

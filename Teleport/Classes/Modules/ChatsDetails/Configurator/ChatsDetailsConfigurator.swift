//
//  ChatsDetailsChatsDetailsConfigurator.swift
//  Teleport
//
//  Created by VoidPtr on 20/03/2018.
//  Copyright © 2018 kekussoft. All rights reserved.
//

import UIKit

class ChatsDetailsModuleConfigurator {

    func configureModuleForViewInput<UIViewController>(viewInput: UIViewController) {

        if let viewController = viewInput as? ChatsDetailsViewController {
            configure(viewController: viewController)
        }
    }

    private func configure(viewController: ChatsDetailsViewController) {

        let router = ChatsDetailsRouter()

        let presenter = ChatsDetailsPresenter()
        presenter.view = viewController
        presenter.router = router

        let interactor = ChatsDetailsInteractor()
        interactor.output = presenter

        presenter.interactor = interactor
        viewController.output = presenter
    }

}

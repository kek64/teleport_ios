//
//  ChatsDetailsChatsDetailsViewInput.swift
//  Teleport
//
//  Created by VoidPtr on 20/03/2018.
//  Copyright © 2018 kekussoft. All rights reserved.
//

protocol ChatsDetailsViewInput: class {

    /**
        @author VoidPtr
        Setup initial state of the view
    */

    func setupInitialState()
}

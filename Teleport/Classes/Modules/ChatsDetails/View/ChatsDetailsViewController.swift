//
//  ChatsDetailsChatsDetailsViewController.swift
//  Teleport
//
//  Created by VoidPtr on 20/03/2018.
//  Copyright © 2018 kekussoft. All rights reserved.
//

import UIKit

class ChatsDetailsViewController: UIViewController, ChatsDetailsViewInput {

    var output: ChatsDetailsViewOutput!

    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        output.viewIsReady()
    }


    // MARK: ChatsDetailsViewInput
    func setupInitialState() {
    }
}

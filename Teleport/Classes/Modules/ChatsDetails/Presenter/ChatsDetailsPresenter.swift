//
//  ChatsDetailsChatsDetailsPresenter.swift
//  Teleport
//
//  Created by VoidPtr on 20/03/2018.
//  Copyright © 2018 kekussoft. All rights reserved.
//

class ChatsDetailsPresenter: ChatsDetailsModuleInput, ChatsDetailsViewOutput, ChatsDetailsInteractorOutput {

    weak var view: ChatsDetailsViewInput!
    var interactor: ChatsDetailsInteractorInput!
    var router: ChatsDetailsRouterInput!

    func viewIsReady() {

    }
}

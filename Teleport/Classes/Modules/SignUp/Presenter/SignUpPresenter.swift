//
//  SignUpSignUpPresenter.swift
//  Teleport
//
//  Created by VoidPtr on 20/03/2018.
//  Copyright © 2018 kekussoft. All rights reserved.
//

class SignUpPresenter: SignUpModuleInput, SignUpViewOutput, SignUpInteractorOutput {

    weak var view: SignUpViewInput!
    var interactor: SignUpInteractorInput!
    var router: SignUpRouterInput!

    func viewIsReady() {

    }
}

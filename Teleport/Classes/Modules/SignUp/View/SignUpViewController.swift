//
//  SignUpSignUpViewController.swift
//  Teleport
//
//  Created by VoidPtr on 20/03/2018.
//  Copyright © 2018 kekussoft. All rights reserved.
//

import UIKit

class SignUpViewController: UIViewController, SignUpViewInput {

    var output: SignUpViewOutput!

    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        output.viewIsReady()
    }


    // MARK: SignUpViewInput
    func setupInitialState() {
    }
}

//
//  SignUpSignUpConfigurator.swift
//  Teleport
//
//  Created by VoidPtr on 20/03/2018.
//  Copyright © 2018 kekussoft. All rights reserved.
//

import UIKit

class SignUpModuleConfigurator {

    func configureModuleForViewInput<UIViewController>(viewInput: UIViewController) {

        if let viewController = viewInput as? SignUpViewController {
            configure(viewController: viewController)
        }
    }

    private func configure(viewController: SignUpViewController) {

        let router = SignUpRouter()

        let presenter = SignUpPresenter()
        presenter.view = viewController
        presenter.router = router

        let interactor = SignUpInteractor()
        interactor.output = presenter

        presenter.interactor = interactor
        viewController.output = presenter
    }

}

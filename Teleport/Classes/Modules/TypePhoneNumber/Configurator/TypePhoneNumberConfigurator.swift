//
//  TypePhoneNumberTypePhoneNumberConfigurator.swift
//  Teleport
//
//  Created by VoidPtr on 20/03/2018.
//  Copyright © 2018 kekussoft. All rights reserved.
//

import UIKit

class TypePhoneNumberModuleConfigurator {

    func configureModuleForViewInput<UIViewController>(viewInput: UIViewController) {

        if let viewController = viewInput as? TypePhoneNumberViewController {
            configure(viewController: viewController)
        }
    }

    private func configure(viewController: TypePhoneNumberViewController) {

        let router = TypePhoneNumberRouter()

        let presenter = TypePhoneNumberPresenter()
        presenter.view = viewController
        presenter.router = router

        let interactor = TypePhoneNumberInteractor()
        interactor.output = presenter
        interactor.userService = UserService()

        presenter.interactor = interactor
        viewController.output = presenter
    }

}

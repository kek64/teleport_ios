//
//  TypePhoneNumberTypePhoneNumberPresenter.swift
//  Teleport
//
//  Created by VoidPtr on 20/03/2018.
//  Copyright © 2018 kekussoft. All rights reserved.
//

class TypePhoneNumberPresenter: TypePhoneNumberModuleInput, TypePhoneNumberViewOutput, TypePhoneNumberInteractorOutput {

    weak var view: TypePhoneNumberViewInput!
    var interactor: TypePhoneNumberInteractorInput!
    var router: TypePhoneNumberRouterInput!

    func viewIsReady() {
        view.setupInitialState()
    }

    func verify(_ phoneNumber: String?) {
        guard let phoneNumber = phoneNumber else {
            view.showError("Not valid phone number")
            return
        }

        interactor.verify(phoneNumber)
    }

    func userNotExists(_ phoneNumber: String) {
        router.register(phoneNumber: phoneNumber)
    }

    func userAlreadyExists(_ phoneNumber: String) {
        router.login(phoneNumber: phoneNumber)
    }

}

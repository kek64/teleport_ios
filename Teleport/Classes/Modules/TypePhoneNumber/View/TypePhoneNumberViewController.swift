//
//  TypePhoneNumberTypePhoneNumberViewController.swift
//  Teleport
//
//  Created by VoidPtr on 20/03/2018.
//  Copyright © 2018 kekussoft. All rights reserved.
//

import UIKit

final class TypePhoneNumberViewController: UIViewController, TypePhoneNumberViewInput {

    // MARK: - Properties

    var output: TypePhoneNumberViewOutput!

    private lazy var phoneNumberText: UITextField = {
        let textField = UITextField()
        textField.font = UIFont.boldSystemFont(ofSize: 16.0)
        textField.textColor = .white
        textField.backgroundColor = .darkGray
        textField.keyboardType = .phonePad
        return textField
    }()

    private lazy var checkButton: UIButton = {
        let button = UIButton()
        button.setTitle("Next", for: .normal)
        button.addTarget(self, action: #selector(actionVerify(_:)), for: .touchUpInside)
        return button
    }()

    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        output.viewIsReady()
    }

    // MARK: TypePhoneNumberViewInput
    func setupInitialState() {
        view.backgroundColor = .black
        view.addSubview(phoneNumberText)
        phoneNumberText.anchorCenterSuperview()
        phoneNumberText.anchor(left: view.leftAnchor, right: view.rightAnchor, leftConstant: 40.0, rightConstant: 40.0)
    }

    func showError(_ error: String) {
        showMessage("Error", error)
    }

    // MAKR: - Actions

    @objc
    private func actionVerify(_ sender: Any) {
        output.verify(phoneNumberText.text)
    }

}

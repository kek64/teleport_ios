//
//  TypePhoneNumberTypePhoneNumberInteractorOutput.swift
//  Teleport
//
//  Created by VoidPtr on 20/03/2018.
//  Copyright © 2018 kekussoft. All rights reserved.
//

import Foundation

protocol TypePhoneNumberInteractorOutput: class {

    func userAlreadyExists(_ phoneNumber: String)
    func userNotExists(_ phoneNumber: String)
    
}

//
//  ContactsListContactsListPresenter.swift
//  Teleport
//
//  Created by VoidPtr on 20/03/2018.
//  Copyright © 2018 kekussoft. All rights reserved.
//

class ContactsListPresenter: ContactsListModuleInput, ContactsListViewOutput, ContactsListInteractorOutput {

    weak var view: ContactsListViewInput!
    var interactor: ContactsListInteractorInput!
    var router: ContactsListRouterInput!

    private var contacts: [ContactViewModel] = []

    func viewIsReady() {
        view.setTitle("Contacts")
        view.setupInitialState()
    }

    func fetchContacts() {
        interactor.fetchContacts()
    }

    func present(contacts: [ContactViewModel]) {
        self.contacts = contacts
        if contacts.isEmpty {
            view.showEmptyPlaceholder()
        }
        view.refreshView()
    }

    func numberOfSections() -> Int {
        return 1
    }

    func numberOfItems(in section: Int) -> Int {
        return contacts.count
    }

    func contact(at index: Int, section: Int) -> ContactViewModel {
        return contacts[index]
    }

    func select(row: Int, section: Int) {
        let user = contacts[row].user

        if user.hasChatHistory {
            router.showContactChat(contactId: user.userId)
        } else {
            router.showContactDetails(contactId: user.userId)
        }
    }

    func show(error: String) {
        view.showError(error)
    }
}

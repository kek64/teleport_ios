//
//  ContactsListContactsListInteractor.swift
//  Teleport
//
//  Created by VoidPtr on 20/03/2018.
//  Copyright © 2018 kekussoft. All rights reserved.
//

class ContactsListInteractor: ContactsListInteractorInput {

    weak var output: ContactsListInteractorOutput!

    private let contactsService: ContactsService

    init(contactsService: ContactsService) {
        self.contactsService = contactsService
    }

    func fetchContacts() {
        contactsService.fetchDeviceContacts { [weak self] (response: ResponseData<[ContactViewModel]>) in
            guard let strongSelf = self else {
                return
            }

            switch response {
            case .success(let contacts):
                strongSelf.output.present(contacts: contacts)
            case .failure(let error):
                strongSelf.output.show(error: error.errorDescription!)
            }
        }
    }

}

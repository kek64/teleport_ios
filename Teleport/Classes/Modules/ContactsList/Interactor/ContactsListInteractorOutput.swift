//
//  ContactsListContactsListInteractorOutput.swift
//  Teleport
//
//  Created by VoidPtr on 20/03/2018.
//  Copyright © 2018 kekussoft. All rights reserved.
//

import Foundation

protocol ContactsListInteractorOutput: class {

    func present(contacts: [ContactViewModel])
    func show(error: String)

}

//
//  ContactsListContactsListViewController.swift
//  Teleport
//
//  Created by VoidPtr on 20/03/2018.
//  Copyright © 2018 kekussoft. All rights reserved.
//

import UIKit

class ContactsListViewController: UIViewController, ContactsListViewInput,
    UISearchResultsUpdating, UISearchBarDelegate {

    var output: ContactsListViewOutput!

    private var communication: WebSocketService!

    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.backgroundColor = .black
        tableView.backgroundView = UIView()
        tableView.backgroundView?.backgroundColor = .black
        tableView.separatorStyle = .none
        tableView.dataSource = self
        tableView.delegate = self
        view.addSubview(tableView)
        tableView.fillSuperview()
        return tableView
    }()

    fileprivate lazy var searchController: UISearchController = {
        let controller = UISearchController(searchResultsController: nil)
        controller.searchResultsUpdater = self
        controller.dimsBackgroundDuringPresentation = false
        controller.obscuresBackgroundDuringPresentation = false
        controller.searchBar.placeholder = "Search contacts"
        controller.searchBar.tintColor = .white
        controller.searchBar.keyboardAppearance = .dark
        controller.searchBar.delegate = self
        controller.isActive = true
        tabBarController?.definesPresentationContext = true
        return controller
    }()

    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        output.viewIsReady()

        //let reconnectionPolicy = CommunicationServiceReconnectionPolicy.periodicaly(fireTime: TimeInterval(20.0))
       // communication = CommunicationService(reconnectionPolicy: reconnectionPolicy)
        communication = WebSocketService()
    }


    // MARK: ContactsListViewInput
    func setupInitialState() {
        navigationItem.searchController = searchController
        tableView.register(ContactTableViewCell.self, forCellReuseIdentifier: ContactTableViewCell.identifier)
        output.fetchContacts()
    }

    func setTitle(_ title: String) {
        self.title = title
    }

    func refreshView() {
        tableView.reloadData()
    }

    func showEmptyPlaceholder() {
        
    }

    func showError(_ error: String) {
        showMessage("Error", error)
    }

    // MARK: - UISearchResultUpdating

    func updateSearchResults(for searchController: UISearchController) {

    }

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if let communication = communication {
            communication.send(message: searchController.searchBar.text ?? "Simple message")
        }
    }

}

// MARK: - UITableViewDataSource, UITableViewDelegate
extension ContactsListViewController: UITableViewDataSource, UITableViewDelegate {

    func numberOfSections(in tableView: UITableView) -> Int {
        return output.numberOfSections()
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return output.numberOfItems(in: section)
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ContactTableViewCell.identifier, for: indexPath) as? ContactTableViewCell else {
            return UITableViewCell()
        }

        cell.data = output.contact(at: indexPath.row, section: indexPath.section)
        let colorCount = AppConstants.Colors.allValues.count
        cell.containerView.backgroundColor = AppConstants.Colors.allValues[ indexPath.row % colorCount ]

        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        output.select(row: indexPath.row, section: indexPath.section)
    }

}

//
//  ContactsListContactsListViewInput.swift
//  Teleport
//
//  Created by VoidPtr on 20/03/2018.
//  Copyright © 2018 kekussoft. All rights reserved.
//

protocol ContactsListViewInput: class {

    /**
        @author VoidPtr
        Setup initial state of the view
    */

    func setupInitialState()
    func refreshView()
    func showEmptyPlaceholder()
    func setTitle(_ title: String)
    func showError(_ error: String)
}

//
//  ContactsListContactsListViewOutput.swift
//  Teleport
//
//  Created by VoidPtr on 20/03/2018.
//  Copyright © 2018 kekussoft. All rights reserved.
//

protocol ContactsListViewOutput {

    /**
        @author VoidPtr
        Notify presenter that view is ready
    */

    func viewIsReady()

    func fetchContacts()

    func numberOfSections() -> Int
    func numberOfItems(in section: Int) -> Int
    func contact(at index: Int, section: Int) -> ContactViewModel
    func select(row: Int, section: Int)
    
}

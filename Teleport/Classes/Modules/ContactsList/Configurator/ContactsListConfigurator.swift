//
//  ContactsListContactsListConfigurator.swift
//  Teleport
//
//  Created by VoidPtr on 20/03/2018.
//  Copyright © 2018 kekussoft. All rights reserved.
//

import UIKit

class ContactsListModuleConfigurator {

    func configureModuleForViewInput<UIViewController>(viewInput: UIViewController) {

        if let viewController = viewInput as? ContactsListViewController {
            configure(viewController: viewController)
        }
    }

    private func configure(viewController: ContactsListViewController) {

        let router = ContactsListRouter()
        router.transferViewController = viewController

        let presenter = ContactsListPresenter()
        presenter.view = viewController
        presenter.router = router

        let contactsService = ContactsService()
        let interactor = ContactsListInteractor(contactsService: contactsService)
        interactor.output = presenter

        presenter.interactor = interactor
        viewController.output = presenter
    }

}

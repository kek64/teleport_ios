//
//  ContactsListContactsListRouter.swift
//  Teleport
//
//  Created by VoidPtr on 20/03/2018.
//  Copyright © 2018 kekussoft. All rights reserved.
//

import UIKit

final class ContactsListRouter: ContactsListRouterInput {

    weak var transferViewController: UIViewController?

    func showContactChat(contactId: String) {

    }

    func showContactDetails(contactId: String) {
        let vc = ContactDetailsViewController()
        let configurator = ContactDetailsModuleConfigurator()
        configurator.configureModuleForViewInput(viewInput: vc, contactId: contactId)
        transferViewController?.navigationController?.pushAndHideBottomBar(vc, animated: true)
    }

}

//
//  SignInSignInConfigurator.swift
//  Teleport
//
//  Created by VoidPtr on 20/03/2018.
//  Copyright © 2018 kekussoft. All rights reserved.
//

import UIKit

class SignInModuleConfigurator {

    func configureModuleForViewInput<UIViewController>(viewInput: UIViewController) {

        if let viewController = viewInput as? SignInViewController {
            configure(viewController: viewController)
        }
    }

    private func configure(viewController: SignInViewController) {

        let router = SignInRouter()

        let presenter = SignInPresenter()
        presenter.view = viewController
        presenter.router = router

        let interactor = SignInInteractor()
        interactor.output = presenter

        presenter.interactor = interactor
        viewController.output = presenter
    }

}

//
//  SignInSignInPresenter.swift
//  Teleport
//
//  Created by VoidPtr on 20/03/2018.
//  Copyright © 2018 kekussoft. All rights reserved.
//

class SignInPresenter: SignInModuleInput, SignInViewOutput, SignInInteractorOutput {

    weak var view: SignInViewInput!
    var interactor: SignInInteractorInput!
    var router: SignInRouterInput!

    func viewIsReady() {

    }
}

//
//  SignInSignInViewController.swift
//  Teleport
//
//  Created by VoidPtr on 20/03/2018.
//  Copyright © 2018 kekussoft. All rights reserved.
//

import UIKit

class SignInViewController: UIViewController, SignInViewInput {

    var output: SignInViewOutput!

    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        output.viewIsReady()
    }


    // MARK: SignInViewInput
    func setupInitialState() {
    }
}

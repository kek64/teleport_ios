//
//  ChatsListChatsListConfigurator.swift
//  Teleport
//
//  Created by VoidPtr on 20/03/2018.
//  Copyright © 2018 kekussoft. All rights reserved.
//

import UIKit

class ChatsListModuleConfigurator {

    func configureModuleForViewInput<UIViewController>(viewInput: UIViewController) {

        if let viewController = viewInput as? ChatsListViewController {
            configure(viewController: viewController)
        }
    }

    private func configure(viewController: ChatsListViewController) {

        let router = ChatsListRouter()

        let presenter = ChatsListPresenter()
        presenter.view = viewController
        presenter.router = router

        let chatService = ChatService()
        let interactor = ChatsListInteractor(chatService: chatService)
        interactor.output = presenter

        presenter.interactor = interactor
        viewController.output = presenter
    }

}

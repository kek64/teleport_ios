//
//  ChatsListChatsListPresenter.swift
//  Teleport
//
//  Created by VoidPtr on 20/03/2018.
//  Copyright © 2018 kekussoft. All rights reserved.
//

class ChatsListPresenter: ChatsListModuleInput, ChatsListViewOutput, ChatsListInteractorOutput {

    weak var view: ChatsListViewInput!
    var interactor: ChatsListInteractorInput!
    var router: ChatsListRouterInput!

    private var chats: [ChatViewModel] = []

    func viewIsReady() {
        view.setTitle("Chats")
        view.setupInitialState()
    }

    func fetchChats() {
        interactor.fetchChats()
    }

    func present(chats: [ChatViewModel]) {
        self.chats = chats
        view.refreshView()
    }

    func numberOfSections() -> Int {
        return 1
    }

    func numberOfItems(in section: Int) -> Int {
        return chats.count
    }

    func chat(at index: Int, section: Int) -> ChatViewModel {
        return chats[index]
    }

}

//
//  ChatsListChatsListViewController.swift
//  Teleport
//
//  Created by VoidPtr on 20/03/2018.
//  Copyright © 2018 kekussoft. All rights reserved.
//

import UIKit

final class ChatsListViewController: UIViewController, ChatsListViewInput {

    var output: ChatsListViewOutput!

    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.backgroundColor = .black
        tableView.backgroundView = UIView()
        tableView.backgroundView?.backgroundColor = .black
        tableView.separatorStyle = .none
        tableView.dataSource = self
        tableView.delegate = self
        view.addSubview(tableView)
        tableView.fillSuperview()
        return tableView
    }()

    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        output.viewIsReady()
    }


    // MARK: ChatsListViewInput
    func setupInitialState() {
        tableView.register(ChatTableViewCell.self, forCellReuseIdentifier: ChatTableViewCell.identifier)
        output.fetchChats()
    }

    func setTitle(_ title: String) {
        self.title = title
    }

    func refreshView() {
        tableView.reloadData()
    }

    func showEmptyPlaceholder() {

    }

}

// MARK: - UITableViewDataSource, UITableViewDelegate
extension ChatsListViewController: UITableViewDataSource, UITableViewDelegate {

    func numberOfSections(in tableView: UITableView) -> Int {
        return output.numberOfSections()
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return output.numberOfItems(in: section)
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ChatTableViewCell.identifier, for: indexPath) as? ChatTableViewCell else {
            return UITableViewCell()
        }

        cell.data = output.chat(at: indexPath.row, section: indexPath.section)
        let colorCount = AppConstants.Colors.allValues.count
        cell.containerView.backgroundColor = AppConstants.Colors.allValues[ indexPath.row % colorCount ]

        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }

}

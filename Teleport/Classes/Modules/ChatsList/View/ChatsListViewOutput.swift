//
//  ChatsListChatsListViewOutput.swift
//  Teleport
//
//  Created by VoidPtr on 20/03/2018.
//  Copyright © 2018 kekussoft. All rights reserved.
//

protocol ChatsListViewOutput {

    /**
        @author VoidPtr
        Notify presenter that view is ready
    */

    func viewIsReady()
    func fetchChats()
    func numberOfSections() -> Int
    func numberOfItems(in section: Int) -> Int
    func chat(at index: Int, section: Int) -> ChatViewModel
}

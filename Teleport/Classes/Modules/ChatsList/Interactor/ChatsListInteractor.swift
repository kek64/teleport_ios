//
//  ChatsListChatsListInteractor.swift
//  Teleport
//
//  Created by VoidPtr on 20/03/2018.
//  Copyright © 2018 kekussoft. All rights reserved.
//

class ChatsListInteractor: ChatsListInteractorInput {

    weak var output: ChatsListInteractorOutput!

    private var chatService = ChatService()

    init(chatService: ChatService) {
        self.chatService = chatService
    }

    func fetchChats() {
        var result: [ChatViewModel] = []
        for _ in 0 ..< 15 {
            result.append(ChatViewModel())
        }
        output.present(chats: result)
    }
}

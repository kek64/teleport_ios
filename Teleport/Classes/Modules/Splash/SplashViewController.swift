//
//  SplashViewController.swift
//  Teleport
//
//  Created by VoidPtr on 3/20/18.
//  Copyright © 2018 VoidPtr. All rights reserved.
//

import UIKit

final class SplashViewController: UIViewController {

    // MAKR: - Properties

    private struct Constants {
        static let titleText = "Wellcome to Teleport Mesenger"
        static let continueText = "Continue"
    }

    private lazy var titleLabel: UILabel = {
        let label = UILabel.defaultBoldLabel(with: 44.0)
        label.numberOfLines = 0
        label.textAlignment = .center
        label.text = Constants.titleText
        return label
    }()

    private lazy var appImageView = UIImageView(image: UIImage(named: "AppIcon"))

    private lazy var continueButton: UIButton = {
        let button = UIButton()
        button.setTitle(Constants.continueText, for: .normal)
        button.addTarget(self, action: #selector(actionContinue(_:)), for: .touchUpInside)
        return button
    }()

    // MARK: - Life-Cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        configurateViewController()
    }

    // MARK: - Configurations

    private func configurateViewController() {
        navigationController?.isNavigationBarHidden = true
        
        view.addSubview(titleLabel)
        titleLabel.anchor(view.safeAreaLayoutGuide.topAnchor, left: view.leftAnchor, right: view.rightAnchor,
                     topConstant: 74.0, leftConstant: 16.0, rightConstant: 16.0)

        view.addSubview(appImageView)
        appImageView.anchorCenterXToSuperview()
        appImageView.anchor(titleLabel.bottomAnchor, topConstant: 22.0, widthConstant: 64.0, heightConstant: 64.0)

        view.addSubview(continueButton)
        continueButton.anchorCenterXToSuperview()
        continueButton.anchor(bottom: view.safeAreaLayoutGuide.bottomAnchor, bottomConstant: 68.0, heightConstant: 48.0)
    }

    // MARK: - Actions

    @objc
    func actionContinue(_ sender: Any) {
        let vc = TypePhoneNumberViewController()
        let configurator = TypePhoneNumberModuleConfigurator()
        configurator.configureModuleForViewInput(viewInput: vc)
        navigationController?.setViewControllers([vc], animated: true)
    }

}

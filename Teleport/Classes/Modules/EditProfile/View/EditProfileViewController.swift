//
//  EditProfileEditProfileViewController.swift
//  Teleport
//
//  Created by VoidPtr on 20/03/2018.
//  Copyright © 2018 kekussoft. All rights reserved.
//

import UIKit

class EditProfileViewController: UIViewController, EditProfileViewInput {

    var output: EditProfileViewOutput!

    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        output.viewIsReady()
    }


    // MARK: EditProfileViewInput
    func setupInitialState() {
    }
}

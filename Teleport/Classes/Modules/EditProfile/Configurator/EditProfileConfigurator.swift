//
//  EditProfileEditProfileConfigurator.swift
//  Teleport
//
//  Created by VoidPtr on 20/03/2018.
//  Copyright © 2018 kekussoft. All rights reserved.
//

import UIKit

class EditProfileModuleConfigurator {

    func configureModuleForViewInput<UIViewController>(viewInput: UIViewController) {

        if let viewController = viewInput as? EditProfileViewController {
            configure(viewController: viewController)
        }
    }

    private func configure(viewController: EditProfileViewController) {

        let router = EditProfileRouter()

        let presenter = EditProfilePresenter()
        presenter.view = viewController
        presenter.router = router

        let interactor = EditProfileInteractor()
        interactor.output = presenter

        presenter.interactor = interactor
        viewController.output = presenter
    }

}

//
//  MessangerMessangerPresenter.swift
//  Teleport
//
//  Created by VoidPtr on 20/03/2018.
//  Copyright © 2018 kekussoft. All rights reserved.
//

class MessangerPresenter: MessangerModuleInput, MessangerViewOutput, MessangerInteractorOutput {

    weak var view: MessangerViewInput!
    var interactor: MessangerInteractorInput!
    var router: MessangerRouterInput!

    func viewIsReady() {

    }
}

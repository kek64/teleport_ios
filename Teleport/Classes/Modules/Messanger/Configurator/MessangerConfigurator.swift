//
//  MessangerMessangerConfigurator.swift
//  Teleport
//
//  Created by VoidPtr on 20/03/2018.
//  Copyright © 2018 kekussoft. All rights reserved.
//

import UIKit

class MessangerModuleConfigurator {

    func configureModuleForViewInput<UIViewController>(viewInput: UIViewController) {

        if let viewController = viewInput as? MessangerViewController {
            configure(viewController: viewController)
        }
    }

    private func configure(viewController: MessangerViewController) {

        let router = MessangerRouter()

        let presenter = MessangerPresenter()
        presenter.view = viewController
        presenter.router = router

        let interactor = MessangerInteractor()
        interactor.output = presenter

        presenter.interactor = interactor
        viewController.output = presenter
    }

}

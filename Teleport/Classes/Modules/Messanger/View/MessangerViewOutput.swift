//
//  MessangerMessangerViewOutput.swift
//  Teleport
//
//  Created by VoidPtr on 20/03/2018.
//  Copyright © 2018 kekussoft. All rights reserved.
//

protocol MessangerViewOutput {

    /**
        @author VoidPtr
        Notify presenter that view is ready
    */

    func viewIsReady()
}

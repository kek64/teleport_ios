//
//  ChannelsListChannelsListPresenter.swift
//  Teleport
//
//  Created by VoidPtr on 20/03/2018.
//  Copyright © 2018 kekussoft. All rights reserved.
//

class ChannelsListPresenter: ChannelsListModuleInput, ChannelsListViewOutput, ChannelsListInteractorOutput {

    weak var view: ChannelsListViewInput!
    var interactor: ChannelsListInteractorInput!
    var router: ChannelsListRouterInput!

    func viewIsReady() {

    }
}

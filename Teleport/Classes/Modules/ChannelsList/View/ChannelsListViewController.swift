//
//  ChannelsListChannelsListViewController.swift
//  Teleport
//
//  Created by VoidPtr on 20/03/2018.
//  Copyright © 2018 kekussoft. All rights reserved.
//

import UIKit

class ChannelsListViewController: UIViewController, ChannelsListViewInput {

    var output: ChannelsListViewOutput!

    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        output.viewIsReady()
    }


    // MARK: ChannelsListViewInput
    func setupInitialState() {
    }
}

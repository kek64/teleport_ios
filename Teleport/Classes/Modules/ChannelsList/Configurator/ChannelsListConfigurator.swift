//
//  ChannelsListChannelsListConfigurator.swift
//  Teleport
//
//  Created by VoidPtr on 20/03/2018.
//  Copyright © 2018 kekussoft. All rights reserved.
//

import UIKit

class ChannelsListModuleConfigurator {

    func configureModuleForViewInput<UIViewController>(viewInput: UIViewController) {

        if let viewController = viewInput as? ChannelsListViewController {
            configure(viewController: viewController)
        }
    }

    private func configure(viewController: ChannelsListViewController) {

        let router = ChannelsListRouter()

        let presenter = ChannelsListPresenter()
        presenter.view = viewController
        presenter.router = router

        let interactor = ChannelsListInteractor()
        interactor.output = presenter

        presenter.interactor = interactor
        viewController.output = presenter
    }

}

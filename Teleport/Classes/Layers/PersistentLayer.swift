//
//  PersistentLayer.swift
//  Teleport
//
//  Created by VoidPtr on 3/20/18.
//  Copyright © 2018 VoidPtr. All rights reserved.
//

import CoreData
import UIKit

final class PersistentLayer {

    // MARK: - Properties

    static var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "Teleport")
        container.loadPersistentStores(completionHandler: { _, error in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    private struct Constants {
        static let cachedImagesFolder = "/CachedImages"
    }

    private let manager: FileManager
    private let dateFormatter: DateFormatter

    private lazy var cachedImagesDirectory: String = {
        let documentPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
        return documentPath + Constants.cachedImagesFolder
    }()

    // MARK: - Life-Cycle

    init() {
        self.manager = FileManager.default
        self.dateFormatter = DateFormatter()
        self.dateFormatter.dateFormat = "yyyytoss"
        self.createDirectoriesIfNeeded()
    }

    // MARK: - Public

    static func saveContext () {
        if persistentContainer.viewContext.hasChanges {
            do {
                try persistentContainer.viewContext.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

    static func clearCaches() {

    }

}

// MARK: - Document Directory
extension PersistentLayer {

    fileprivate func createDirectoriesIfNeeded() {
        if let documentPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first {
            if !manager.fileExists(atPath: cachedImagesDirectory) {
                do {
                    let path = documentPath + Constants.cachedImagesFolder
                    let directoryURL = URL(fileURLWithPath: path)
                    try manager.createDirectory(at: directoryURL, withIntermediateDirectories: false, attributes: nil)
                } catch {
                    print(error)
                }
            }
        }
    }

    func store(imageData: Data, url: URL) -> String? { // ok
        let imagePath = "\(cachedImagesDirectory)/\(url.lastPathComponent)"
        try? imageData.write(to: URL(fileURLWithPath: imagePath))
        return imagePath
    }

    func store(image: UIImage, filename: String) -> String? {
        let imagePath = "\(cachedImagesDirectory)/\(filename)"
        try? UIImagePNGRepresentation(image)?.write(to: URL(fileURLWithPath: imagePath))
        return imagePath
    }

    func fetch(path: String) -> UIImage? {
        let pathURL = URL(fileURLWithPath: path)
        return fetch(url: pathURL)
    }

    func fetch(url: URL) -> UIImage? { // ok
        var resultImage: UIImage? = nil
        let imagePath = URL(fileURLWithPath: "\(cachedImagesDirectory)/\(url.lastPathComponent)")
        do {
            let imageData = try Data(contentsOf: imagePath)
            if let fileImage = UIImage(data: imageData) {
                UIGraphicsBeginImageContext( CGSize( width: 1, height: 1 ))
                let context = UIGraphicsGetCurrentContext()
                context!.draw(fileImage.cgImage!, in: CGRect( x: 0, y: 0, width: 1, height: 1 ))
                UIGraphicsEndImageContext()
                resultImage = fileImage
            }
        } catch {
            return resultImage
        }
        return resultImage
    }

    func remove(imagePath: String) {
        do {
            let pathURL = URL(fileURLWithPath: imagePath)
            let url = URL(fileURLWithPath: "\(cachedImagesDirectory)/\(pathURL.lastPathComponent)")
            try manager.removeItem(at: url)
        } catch {
            print("Failed remove data at path \(imagePath) with error \(error)")
        }
    }

}

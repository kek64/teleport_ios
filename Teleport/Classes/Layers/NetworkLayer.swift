//
//  NetworkLayer.swift
//  Teleport
//
//  Created by VoidPtr on 3/20/18.
//  Copyright © 2018 VoidPtr. All rights reserved.
//

import UIKit

enum HTTPMethod: String {
    case post
    case put
    case get
    case delete
    case patch
    case trace
    case connect
}

private struct NetworkConstants {
    static let baseURL = URL(string: "http://teleport-backend.herokuapp.com")!
}

protocol EndpointProtocol {
    var parameters: [String: Any]? { get }
    var path: String { get }
    var method: HTTPMethod { get }
    var multipartParameters: [String: Any]? { get }
    var multipartFormat: [String: String]? { get }
}

enum ResponseData<T> {
    case success(T)
    case failure(AppError)
}

final class NetworkManager {

    // MARK: - Properties

    private let sessionToken: String?

    // MARK: - Life-Cycle

    init(sessionToken: String?) {
        self.sessionToken = sessionToken
    }

    // MARK: - Public

    func performRequest<T: Codable>(session: URLSession = URLSession.shared,
                                    endpoint: EndpointProtocol,
                                    _ completion: ((ResponseData<T>) -> Void)?) {
        let requestURL = NetworkConstants.baseURL.appendingPathComponent(endpoint.path)
        var request = URLRequest(url: requestURL)

        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Accept")
        request.httpMethod = endpoint.method.rawValue.uppercased()

        if let sessionToken = sessionToken {
            request.setValue(sessionToken, forHTTPHeaderField: "Session-Token")
        }

        if let params = endpoint.parameters {
            request.httpBody = try? JSONSerialization.data(withJSONObject: params, options: [])
        }

        session.dataTask(with: request) { data, _, error in
            guard error == nil else {
                print(error?.localizedDescription ?? "Something went wrong")
                DispatchQueue.main.async {
                    let err = AppError.unexpected(errorString: error!.localizedDescription)
                    completion?(.failure(err))
                }
                return
            }

            if let data = data {
                if let decodedData = try? JSONDecoder().decode(T.self, from: data) {
                    DispatchQueue.main.async {
                        print(decodedData)
                        completion?(.success(decodedData))
                    }
                }
            }
        }.resume()
    }

}

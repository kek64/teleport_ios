//
//  KeychainLayer.swift
//  Teleport
//
//  Created by VoidPtr on 3/20/18.
//  Copyright © 2018 VoidPtr. All rights reserved.
//

import Foundation
import Security

final class KeychainLayer {

    // MARK: - Properties

    private struct Constants {
        static let sessionToken = "kSessionToken"
    }

    var sessionToken: String? {
        get {
            return self.get(Constants.sessionToken)
        } set {
            self.set(value: newValue, forKey: Constants.sessionToken)
        }
    }

    // MARK: - Public

    func get(_ key: String) -> String? {
        return UserDefaults.standard.string(forKey: key)
    }

    func set<T>(value: T, forKey: String) {
        UserDefaults.standard.set(value, forKey: forKey)
        _ = UserDefaults.standard.synchronize()
    }

    // MARK: - Private

}

//
//  CommunicationLayer.swift
//  Teleport
//
//  Created by VoidPtr on 3/20/18.
//  Copyright © 2018 VoidPtr. All rights reserved.
//

import Foundation
import CFNetwork

protocol CommunicationLayerSerializerProtocol: class {
    func deserialize(stream: InputStream, onSuccess: @escaping ([String : Any]?) -> Void, onError: @escaping (Error) -> Void)
    func serialize(stream: OutputStream, message: [String: Any]?, onSuccess: @escaping () -> Void, onError: @escaping (Error) -> Void)
}

final class CommunicationLayerJSONSerializer: CommunicationLayerSerializerProtocol {
    private struct Constants {
        static let bufferSize = 1024
        static let encoding = String.Encoding.utf8
    }

    func deserialize(stream: InputStream, onSuccess: @escaping ([String : Any]?) -> Void, onError: @escaping (Error) -> Void) {
        var buffer = Array<UInt8>(repeating: 0, count: Constants.bufferSize)

        let bytesRead = stream.read(&buffer, maxLength: Constants.bufferSize)
        if bytesRead >= 0 {
            if let response = NSString(bytes: &buffer, length: bytesRead, encoding: Constants.encoding.rawValue),
                let responseData = response.data(using: Constants.encoding.rawValue) {
                do {
                    if let message = try JSONSerialization.jsonObject(with: responseData, options: []) as? [String: Any] {
                        onSuccess(message)
                    } else {
                        onSuccess(nil)
                    }

                } catch let error {
                    onError(error)
                }
            }
        }
    }

    func serialize(stream: OutputStream, message: [String: Any]?, onSuccess: @escaping () -> Void, onError: @escaping (Error) -> Void) {
        guard let unwrappedMessage = message else {
            onError(NSError(domain: "", code: 0, userInfo: ["Error" : "Try send empty message"]))
            return
        }

        var err: NSError?
        let result = JSONSerialization.writeJSONObject(unwrappedMessage, to: stream, options: [], error: &err)
        if result <= 0, let error = err {
            onError(error)
        } else {
            onSuccess()
        }
    }
}

protocol CommunicationLayerDelegate: class {
    func didConnect(stream: Stream)
    func didDisconnet(stream: Stream, message: String)
    func didReceiveMessage(stream: Stream, message: [String: Any])
    func didDisconnect()
}

final class CommunicationLayer: NSObject {

    // MARK: - Properties

    private var inputStream: InputStream?
    private var outputStream: OutputStream?
    private var serialQueue = DispatchQueue(label: "com.kekussoft.teleport", qos: .default, attributes: .concurrent)
    private var streamQueue = DispatchQueue(label: "com.kekussoft.teleport.stream.queue", qos: .default, attributes: .concurrent)
    private var serializer: CommunicationLayerSerializerProtocol

    private var messageQueue: [ [String : Any] ] = []
    private var host: String
    private var port: Int

    var isConnected: Bool = false
    var isTryingConnect: Bool = false
    weak var delegate: CommunicationLayerDelegate?

    // MARK: - Life-Cycle

    init(host: String, port: Int, serializer: CommunicationLayerSerializerProtocol, delegate: CommunicationLayerDelegate? = nil) {
        self.host = host
        self.port = port
        self.serializer = serializer
        self.delegate = delegate
        super.init()
        self.establishConnect()
    }

    deinit {
        disconnect()
    }

    // MARK: - Public

    func establishConnect() {
        isTryingConnect = true
       // Stream.getStreamsToHost(withName: host, port: port, inputStream: &inputStream, outputStream: &outputStream)

        var readStream: Unmanaged<CFReadStream>?
        var writeStream: Unmanaged<CFWriteStream>?

        CFStreamCreatePairWithSocketToHost(kCFAllocatorDefault, host as CFString, UInt32(port), &readStream, &writeStream)

//        guard let inputStream = readStream?.takeRetainedValue(), let outputStream = writeStream?.takeRetainedValue() else {
//            fatalError("Unanable to estabilish socket connection")
//        }

        self.inputStream = readStream?.takeRetainedValue()
        self.outputStream = writeStream?.takeRetainedValue()

        guard let inputStream = self.inputStream, let outputStream = self.outputStream else {
            return
        }

        inputStream.delegate = self
        outputStream.delegate = self

        //CFReadStreamSetDispatchQueue(inputStream, streamQueue)
        //CFWriteStreamSetDispatchQueue(outputStream, streamQueue)

        inputStream.schedule(in: .main, forMode: .commonModes)
        outputStream.schedule(in: .main, forMode: .commonModes)

        inputStream.open()
        outputStream.open()
    }

    func disconnect() {
        guard let inputStream = inputStream, let outputStream = outputStream else {
            return
        }

        inputStream.delegate = nil
        outputStream.delegate = nil

        inputStream.close()
        outputStream.close()

        inputStream.remove(from: .main, forMode: .commonModes)
        outputStream.remove(from: .main, forMode: .commonModes)

        isConnected = false
    }

    func send(message: [String : Any]) {
        serialQueue.sync { [weak self] in
            self?.messageQueue.append(message)
        }

        writeToStream()
    }

    func send(httpMessage: String) {

    }

    fileprivate func endEncountered(stream: Stream) {
        // TODO: Implementation
    }

    fileprivate func openCompleted(stream: Stream) {
        guard let inputStream = inputStream, let outputStream = outputStream else {
            return
        }

        if inputStream.streamStatus == .open, outputStream.streamStatus == .open {
            isConnected = true
            isTryingConnect = false
            delegate?.didConnect(stream: stream)
        }
    }

    fileprivate func handleIncommingBytes(_ stream: Stream) {
        if let stream = stream as? InputStream {
            DispatchQueue.global().async { [weak self] in
                self?.serializer.deserialize(stream: stream, onSuccess: { [weak self] message in
                    if let message = message {
                        self?.delegate?.didReceiveMessage(stream: stream, message: message)
                    }
                }, onError: { error in
                    print("Failed deserialize json :: \(error.localizedDescription)")
                })
            }
        }
    }

    fileprivate func writeToStream() {
        guard !messageQueue.isEmpty, let outputStream = outputStream, outputStream.hasSpaceAvailable else {
            return
        }

        DispatchQueue.global().async { [weak self] in
            let message = self?.serialQueue.sync {
                return self?.messageQueue.removeLast()
            }

            self?.serializer.serialize(stream: outputStream, message: message, onSuccess: {
                print("Success Send message")
            }, onError: { [weak self] error in
                print("Failed serialize json to binary while send message. :: \(error.localizedDescription)")
                self?.serialQueue.sync {
                    if let message = message {
                        self?.messageQueue.append( message )
                    }
                }
            })

//            do {
//                // TODO: replace this with - writeTo stream ...
//                let data = try JSONSerialization.data(withJSONObject: unwrappedMessage, options: [])
//                var err: NSError?
//
//                JSONSerialization.writeJSONObject(unwrappedMessage, to: outputStream, options: [], error: &err)
//
//                var buffer = Array<UInt8>(repeating: 0, count: data.count)
//                data.copyBytes(to: &buffer, count: data.count * MemoryLayout<UInt8>.size)
//
//                if outputStream.write(&buffer, maxLength: Constants.bufferSize) == -1 {
//                    self?.serialQueue.sync {
//                        self?.messageQueue.append( unwrappedMessage )
//                    }
//                }
//
//            } catch let error {
//                print("Failed serialize json to binary while send message. :: \(error.localizedDescription)")
//            }
        }
    }

    func sendHandshake(host: String, port: Int) {
        DispatchQueue.global().asyncAfter(deadline: DispatchTime.now() + 1.0) { [weak self] in
            let url = URL(string: host)!
            var request = URLRequest(url: url)

            request.setValue(url.host, forHTTPHeaderField: "Host")
            request.setValue("websocket", forHTTPHeaderField: "Upgrade")
            request.setValue("upgrade", forHTTPHeaderField: "Connection")
            request.setValue("localhost", forHTTPHeaderField: "Origin")
            request.setValue("chat, superchat", forHTTPHeaderField: "Sec-WebSocket-Protocol")
           // request.setValue("x3JJHMbDL1EzLkh9GBhXDw==", forHTTPHeaderField: "Sec-WebSocket-Key")
            request.setValue("13", forHTTPHeaderField: "Sec-WebSocket-Version")
            request.setValue("localhost", forHTTPHeaderField: "Sec-WebSocket-Origin")
            //request.setValue("no-cache", forHTTPHeaderField: "Pragma")
           // request.setValue("no-cache", forHTTPHeaderField: "Cache-Control")

            var requestString = "GET \(url.host!) HTTP/1.1\r\n" // \(host)
            for key in request.allHTTPHeaderFields!.keys {
                if let val = request.value(forHTTPHeaderField: key) {
                    requestString += "\(key): \(val)\r\n"
                }
            }

           let data = requestString.data(using: .utf8)!
           var buffer = Array<UInt8>(repeating: 0, count: data.count)
           data.copyBytes(to: &buffer, count: data.count * MemoryLayout<UInt8>.size)

            var header = [UInt8]()
            for byte in requestString.utf8 {
                header.append(byte)
            }

            print("HANDSHAKE HEADER :: \n")
            print(requestString)
            if let outputStream = self?.outputStream {
                if outputStream.write(&buffer, maxLength: data.count) == -1 {
                    print("Failed write handshake to output stream")
                }
            }
            print("\n")
       }
    }

}

// MARK: - StreamDelegate
extension CommunicationLayer: StreamDelegate {

    func stream(_ aStream: Stream, handle eventCode: Stream.Event) {
        switch eventCode {
        case .endEncountered:
            break
        case .errorOccurred:
            isConnected = false
            isTryingConnect = false
            //disconnect()
            print("Stream Error :: \(aStream.streamError?.localizedDescription ?? "")")
            break
        case .hasBytesAvailable:
            isTryingConnect = false
            handleIncommingBytes(aStream)
        case .hasSpaceAvailable:
            isTryingConnect = false
            writeToStream()
        case .openCompleted:
            openCompleted(stream: aStream)
        default:
            break
        }

        switch aStream.streamStatus {
        case .open:
            break
        case .notOpen:
            break
        case .opening:
            break
        case .reading:
            break
        case .writing:
            break
        case .atEnd:
            break
        case .closed:
            break
        case .error:
            break
        }
    }

}

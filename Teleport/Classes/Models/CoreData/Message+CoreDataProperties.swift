//
//  Message+CoreDataProperties.swift
//  Teleport
//
//  Created by VoidPtr on 4/8/18.
//  Copyright © 2018 VoidPtr. All rights reserved.
//
//

import Foundation
import CoreData


extension Message {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Message> {
        return NSFetchRequest<Message>(entityName: "Message")
    }

    @NSManaged public var createdAt: NSDate?
    @NSManaged public var id: Int64
    @NSManaged public var body: String?
    @NSManaged public var chatId: Chat?
    @NSManaged public var senderId: User?

}

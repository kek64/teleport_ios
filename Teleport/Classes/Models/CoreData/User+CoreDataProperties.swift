//
//  User+CoreDataProperties.swift
//  Teleport
//
//  Created by VoidPtr on 4/8/18.
//  Copyright © 2018 VoidPtr. All rights reserved.
//
//

import Foundation
import CoreData


extension User {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<User> {
        return NSFetchRequest<User>(entityName: "User")
    }

    @NSManaged public var alias: String?
    @NSManaged public var details: String?
    @NSManaged public var email: String?
    @NSManaged public var id: Int64
    @NSManaged public var phone: String?
    @NSManaged public var userName: String?
    @NSManaged public var messages: NSSet?
    @NSManaged public var userChats: NSSet?

}

// MARK: Generated accessors for messages
extension User {

    @objc(addMessagesObject:)
    @NSManaged public func addToMessages(_ value: Message)

    @objc(removeMessagesObject:)
    @NSManaged public func removeFromMessages(_ value: Message)

    @objc(addMessages:)
    @NSManaged public func addToMessages(_ values: NSSet)

    @objc(removeMessages:)
    @NSManaged public func removeFromMessages(_ values: NSSet)

}

// MARK: Generated accessors for userChats
extension User {

    @objc(addUserChatsObject:)
    @NSManaged public func addToUserChats(_ value: Chat)

    @objc(removeUserChatsObject:)
    @NSManaged public func removeFromUserChats(_ value: Chat)

    @objc(addUserChats:)
    @NSManaged public func addToUserChats(_ values: NSSet)

    @objc(removeUserChats:)
    @NSManaged public func removeFromUserChats(_ values: NSSet)

}

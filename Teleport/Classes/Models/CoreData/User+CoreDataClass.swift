//
//  User+CoreDataClass.swift
//  Teleport
//
//  Created by VoidPtr on 3/21/18.
//  Copyright © 2018 VoidPtr. All rights reserved.
//
//

import Foundation
import CoreData

extension CodingUserInfoKey {
    static let context = CodingUserInfoKey(rawValue: "context")
}

public class User: NSManagedObject, Codable {
    var password: String = ""

    // MARK: - Codable

    enum CodingKeys: String, CodingKey {
        case userName
        case alias
        case phone
        case email
        case id
        case details
    }

    required convenience public init(from decoder: Decoder) throws {
        guard let contextUserInfoKey = CodingUserInfoKey.context,
            let managedObjectContext = decoder.userInfo[contextUserInfoKey] as? NSManagedObjectContext,
            let entity = NSEntityDescription.entity(forEntityName: String(describing: User.self), in: managedObjectContext) else {
                fatalError("Failed to decode Person!")
        }

        self.init(entity: entity, insertInto: nil)

        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decode(Int64.self, forKey: .id)
        userName = try values.decode(String.self, forKey: .userName)
        alias = try values.decode(String.self, forKey: .alias)
        phone = try values.decode(String.self, forKey: .phone)
        email = try values.decode(String.self, forKey: .email)
        details = try values.decode(String.self, forKey: .details)
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(id, forKey: .id)
        try container.encode(userName, forKey: .userName)
        try container.encode(alias, forKey: .alias)
        try container.encode(phone, forKey: .phone)
        try container.encode(email, forKey: .email)
        try container.encode(details, forKey: .details)
    }

}

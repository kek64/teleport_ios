//
//  Channel+CoreDataProperties.swift
//  Teleport
//
//  Created by VoidPtr on 4/8/18.
//  Copyright © 2018 VoidPtr. All rights reserved.
//
//

import Foundation
import CoreData


extension Channel {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Channel> {
        return NSFetchRequest<Channel>(entityName: "Channel")
    }

    @NSManaged public var alias: String?
    @NSManaged public var details: String?
    @NSManaged public var id: Int64
    @NSManaged public var name: String?
    @NSManaged public var subscribersCount: Int64

}

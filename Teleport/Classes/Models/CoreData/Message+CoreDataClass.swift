//
//  Message+CoreDataClass.swift
//  Teleport
//
//  Created by VoidPtr on 4/8/18.
//  Copyright © 2018 VoidPtr. All rights reserved.
//
//

import Foundation
import CoreData

public class Message: NSManagedObject, Codable {

    // MARK: - Codable

    enum CodingKeys: String, CodingKey {
        case id
        case body
        case senderId
        case chatId
        case createdAt
    }

    required convenience public init(from decoder: Decoder) throws {
        guard let contextUserInfoKey = CodingUserInfoKey.context,
            let managedObjectContext = decoder.userInfo[contextUserInfoKey] as? NSManagedObjectContext,
            let entity = NSEntityDescription.entity(forEntityName: String(describing: User.self), in: managedObjectContext) else {
                fatalError("Failed to decode Person!")
        }

        self.init(entity: entity, insertInto: nil)

        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decode(Int64.self, forKey: .id)
        body = try values.decode(String.self, forKey: .body)
        let _ = try values.decode(Int64.self, forKey: .senderId)
        let _ = try values.decode(Int64.self, forKey: .chatId)
        createdAt = try values.decode(Date.self, forKey: .createdAt) as NSDate
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(id, forKey: .id)
        try container.encode(body, forKey: .body)
        try container.encode(senderId?.id, forKey: .senderId)
        try container.encode(chatId?.id, forKey: .chatId)
       // try container.encode(createdAt as Date, forKey: .createdAt)
    }

}

//
//  Chat+CoreDataClass.swift
//  Teleport
//
//  Created by VoidPtr on 3/21/18.
//  Copyright © 2018 VoidPtr. All rights reserved.
//
//

import Foundation
import CoreData

public class Chat: NSManagedObject, Codable {

    // MARK: - Codable

    enum CodingKeys: String, CodingKey {
        case id
        case name
        case invitedId
    }

    required convenience public init(from decoder: Decoder) throws {
        guard let contextUserInfoKey = CodingUserInfoKey.context,
            let managedObjectContext = decoder.userInfo[contextUserInfoKey] as? NSManagedObjectContext,
            let entity = NSEntityDescription.entity(forEntityName: String(describing: User.self), in: managedObjectContext) else {
                fatalError("Failed to decode Person!")
        }

        self.init(entity: entity, insertInto: nil)

        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decode(Int64.self, forKey: .id)
        name = try values.decode(String.self, forKey: .name)
        invitedId = try values.decode(Int64.self, forKey: .invitedId)
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(id, forKey: .id)
        try container.encode(name, forKey: .name)
        try container.encode(invitedId, forKey: .invitedId)
    }

}

//
//  ResponseMessage.swift
//  Teleport
//
//  Created by VoidPtr on 3/21/18.
//  Copyright © 2018 VoidPtr. All rights reserved.
//

import Foundation

struct ResponseMessage: Codable {
    var errorMessage: String
    var successMessage: String

    enum CodingKeys: String, CodingKey {
        case errorMessage = "Error.message"
        case successMessage = "Success.message"
    }
}

//
//  AuthResponse.swift
//  Teleport
//
//  Created by VoidPtr on 4/8/18.
//  Copyright © 2018 VoidPtr. All rights reserved.
//

import Foundation

struct AuthResponse: Codable {
    var message: String

    enum CodingKeys: String, CodingKey {
        case message
    }
}

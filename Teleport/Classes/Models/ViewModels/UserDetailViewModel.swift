//
//  UserDetailViewModel.swift
//  Teleport
//
//  Created by VoidPtr on 3/22/18.
//  Copyright © 2018 VoidPtr. All rights reserved.
//

import Foundation

struct UserDetailsBaseViewModel {
    var identifier: String = ""
    var title: String = ""
    var subtitle: String = ""
}

struct UserDetails {
    var identifier: String = ""
    var title: String = ""
    var subtitle: String = ""
}

//
//  UserViewModel.swift
//  Teleport
//
//  Created by VoidPtr on 3/21/18.
//  Copyright © 2018 VoidPtr. All rights reserved.
//

import Foundation

struct UserViewModel {
    static let defaultAvatarImageURL = "https://images.petsmartassets.com/is/image/PetSmart/BR_VDFEAT-Authority-DOG-20160818?$PB0802$"

    var userId: String
    var fullName: String
    var aliasName: String
    var phone: String?
    var email: String?
    var avatar: String? = UserViewModel.defaultAvatarImageURL
    var avatarData: Data?
    var chatsCount: Int = 0
    var hasChatHistory: Bool = false

    init(userId: String = "", fullName: String, aliasName: String, phone: String? = nil,
         email: String? = nil, avatar: String? = defaultAvatarImageURL, avatarData: Data? = nil,
         chatsCount: Int = 0, hasChatHistory: Bool = false) {
        self.userId = userId
        self.fullName = fullName
        self.aliasName = aliasName
        self.phone = phone
        self.email = email
        self.avatar = avatar
        self.avatarData = avatarData
        self.chatsCount = chatsCount
        self.hasChatHistory = hasChatHistory
    }
}

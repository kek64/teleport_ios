//
//  ContactViewModel.swift
//  Teleport
//
//  Created by VoidPtr on 3/21/18.
//  Copyright © 2018 VoidPtr. All rights reserved.
//

import Foundation

struct ContactViewModel {
    let status: String = "online"
    let user: UserViewModel
}

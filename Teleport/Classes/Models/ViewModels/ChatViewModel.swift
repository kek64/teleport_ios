//
//  ChatViewModel.swift
//  Teleport
//
//  Created by VoidPtr on 3/21/18.
//  Copyright © 2018 VoidPtr. All rights reserved.
//

import Foundation

struct ChatViewModel {
    let name = "Simple Chat"
    let body = "Last message in current chat."
    let avatarURL = "https://images.petsmartassets.com/is/image/PetSmart/BR_VDFEAT-Authority-DOG-20160818?$PB0802$"
}
